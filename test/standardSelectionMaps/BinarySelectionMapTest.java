package standardSelectionMaps;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyPerformanceTestFileLocations;
import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.MissingFeatureForSelectionException;
import executableAlgorithmSystem.ExecutableAlgorithm;

import performanceDatabase.Instance;

public class BinarySelectionMapTest {

	ExecutableAlgorithm alg1;
	File exec1, exec2;
	String exec1Location = 	ToyPerformanceTestFileLocations.EXEC_LINEAR ; 
 

	ExecutableAlgorithm alg2;
	String exec2Location = 	ToyPerformanceTestFileLocations.EXEC_CONSTANT ; 

	BinarySelectionMap sm;

	Feature f1;
	double crossoverValue = 10;
	
	
	
	@Before
	public void setUp() throws Exception {
		exec1 = new File(exec1Location);
		exec2 = new File(exec2Location);

		alg1 = new ExecutableAlgorithm.Builder("alg1", exec1).build();
		alg2 = new ExecutableAlgorithm.Builder("alg2", exec2).build();
		
		sm = new BinarySelectionMap(alg1, alg2, f1, crossoverValue);
	}

	@Test
	public void testBinarySelectionMap() {
		assertTrue(sm.getAlgorithm1().equals(alg1));
		assertTrue(sm.getAlgorithm2().equals(alg2));
		assertTrue(sm.getCrossoverValue() == crossoverValue);
		assertTrue(sm.getRequiredFeatureValues().contains(f1));
	}

	@Test
	public void testSelectAlgorithm() {
		Instance i1 = new Instance("i1");
		Instance i2 = new Instance("i2");
		
		HashMap<Feature, Double> values1 = new HashMap<Feature, Double>();
		HashMap<Feature, Double> values2 = new HashMap<Feature, Double>();

		values1.put(f1, 9.0);
		values2.put(f1, 11.0);
		
		try {
			assertTrue(sm.selectAlgorithm(i1, values1).equals(alg1));
		} catch (MissingFeatureForSelectionException e) {
			fail();
		}
		try {
			assertTrue(sm.selectAlgorithm(i2, values2).equals(alg2));
		} catch (MissingFeatureForSelectionException e) {
			fail();
		}
		
		boolean thrown = false;
		try {
			sm.selectAlgorithm(i2, new HashMap<Feature, Double>());
		} catch (MissingFeatureForSelectionException e) {
			thrown = true;
		}
		assertTrue(thrown);


		
	}

	@Test
	public void testGetRequiredFeatureValues() {
		assertTrue(sm.getRequiredFeatureValues().size()==1);
		assertTrue(sm.getRequiredFeatureValues().contains(f1));
	}
	
	@Test
	public void testGetSelectableAlgorithms(){
		assertTrue(sm.getSelectableAlgorithms().size()==2);
		assertTrue(sm.getSelectableAlgorithms().contains(alg1));
		assertTrue(sm.getSelectableAlgorithms().contains(alg2));


	}

}
