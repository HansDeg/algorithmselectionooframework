package standardSelectionMaps;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyPerformanceTestFileLocations;
import algorithmSelectionSystem.Feature;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.Instance;

public class SingleAlgorithmSelectionMapTest {

	ExecutableAlgorithm alg;
	SingleAlgorithmSelectionMap sm;
	
	File exec;
	String execLocation = ToyPerformanceTestFileLocations.EXEC_LINEAR ; 

	
	@Before
	public void setUp() throws Exception {
		exec = new File(execLocation);
		alg = new ExecutableAlgorithm.Builder("alg1", exec ).build();
		sm = new SingleAlgorithmSelectionMap(alg);
	}

	@Test
	public void testSelectAlgorithm() {
		Instance i1 = new Instance("i1");
		assertTrue(sm.selectAlgorithm(i1, new HashMap<Feature,Double>()).equals(alg));
		
	}

	@Test
	public void testGetAlgorithm() {
		assertTrue(sm.getAlgorithm().equals(alg));
	}
	
	@Test
	public void testGetSelectableAlgorithms(){
		assertTrue(sm.getSelectableAlgorithms().size()==1);
		assertTrue(sm.getSelectableAlgorithms().contains(alg));

	}

	
}
