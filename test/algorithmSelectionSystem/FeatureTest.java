package algorithmSelectionSystem;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class FeatureTest {

	Feature f1;
	Feature f2;
	Feature f3;
	
	String featureName1 = "sameName";
	String featureName2 = "sameName";
	String featureName3 = "otherName";

	@Before
	public void setUp() throws Exception {
		f1 = new Feature(featureName1);
		f2 = new Feature(featureName2);
		f3 = new Feature(featureName3);
	}
	
	@Test
	public void testFeature(){
		  boolean thrown = false;

		  try {
			  new Feature(null); //Verifies that illegalargumentexception is indeed thrown for null name
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertTrue(thrown);

	}

	@Test
	public void testHashCode() {
		assertTrue(f1.hashCode() == f2.hashCode());
		assertTrue(f2.hashCode() == f1.hashCode());
	}

	@Test
	public void testGetName() {
		assertTrue(f1.getName().equals(featureName1));
	}

	@Test
	public void testEqualsObject() {
		assertTrue(f1.equals(f2));
		assertTrue(f2.equals(f1));
		assertFalse(f1.equals(f3));
		assertFalse(f3.equals(f1));
	}

}
