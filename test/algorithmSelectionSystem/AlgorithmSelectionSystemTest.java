package algorithmSelectionSystem;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyExamplePerformanceExtractor;
import ToyExample.ToyFeatureValueExtractor;
import ToyExample.ToyPerformanceTestFileLocations;
import executableAlgorithmSystem.ExecutableAlgorithm;
import executableAlgorithmSystem.InvalidFileForPerformanceExtractionException;
import executableAlgorithmSystem.PerformanceExtractor;
import executableAlgorithmSystem.RunnableInstance;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import performanceDatabase.Performance;
import standardPerformances.MinimisationPerformance;
import standardSelectionMaps.BinarySelectionMap;
import standardSelectionMaps.SingleAlgorithmSelectionMap;

public class AlgorithmSelectionSystemTest  {

	ExecutableAlgorithm alg1, alg2;
	File exec1,exec2;
	String execLocation1 = ToyPerformanceTestFileLocations.EXEC_LINEAR;
	String execLocation2 = ToyPerformanceTestFileLocations.EXEC_CONSTANT;
	ToyExamplePerformanceExtractor extractor;

	
	RunnableInstance inst;
	File instFile;
	String instFileLocation = ToyPerformanceTestFileLocations.TOY_INST; 

	BinarySelectionMap sm;

	AlgorithmSelectionSystem sys;
	String probName = "toy problem";
	
	String resDirLoc = ToyPerformanceTestFileLocations.RES_DIRECTORY;
	File resDir;
	
	Performance runPerf;
	AlgorithmRun run;
	
	ToyFeatureValueExtractor featExtractor;
	Feature f1;
	
	HashMap<ExecutableAlgorithm, PerformanceExtractor> algorithms;
	HashMap<Feature, FeatureValueExtractor> features;
	
	double smCrossoverValue = 100;

	FeatureValueRecord featureValueRecord;

	@Before
	public void setUp() throws Exception {
		assertTrue(new File(execLocation1).exists());
		assertTrue(new File(execLocation2).exists());
		assertTrue(new File(instFileLocation).exists());
		assertTrue(new File(resDirLoc).exists());
		
		ArrayList<String> prefixes = new ArrayList<String>();
		prefixes.add("java");
		prefixes.add("-jar");
		

		exec1 = new File(execLocation1);
		exec2 = new File(execLocation2);
		alg1 = new ExecutableAlgorithm.Builder("alg1", exec1)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();
		alg2 = new ExecutableAlgorithm.Builder("alg2", exec2)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();

		 
		instFile = new File(instFileLocation);
		inst = new RunnableInstance("i1", instFile);
		
		extractor = new ToyExamplePerformanceExtractor();
		
		resDir = new File(resDirLoc);
		
		
		f1 = new Feature("problem size");
		sm = new BinarySelectionMap(alg1, alg2, f1, smCrossoverValue);

		algorithms = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		algorithms.put(alg1,  extractor);
		algorithms.put(alg2, extractor);
		
		featExtractor = new ToyFeatureValueExtractor();
		features = new HashMap<Feature, FeatureValueExtractor>();
		features.put(f1, featExtractor);
		
		sys = new AlgorithmSelectionSystem.Builder(probName, resDir).performanceExtractors(algorithms).featureValueExtractorMap(features).selectionMap(sm).build();
		
		
		runPerf = new MinimisationPerformance(10);

		run = new AlgorithmRun(alg1, inst, runPerf);

		featureValueRecord = new FeatureValueRecord(inst, f1, featExtractor.extractValue(inst) );		
	}
	

	@Test
	public void testAlgorithmSelectionSystemWithoutFeatures() {
		//Empty init feat, 1 alg tests
		
		//Test: alg required by selection map not specified
		HashMap<Feature, FeatureValueExtractor> features2 = new HashMap<Feature, FeatureValueExtractor>();
		HashMap<ExecutableAlgorithm, PerformanceExtractor> algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		SingleAlgorithmSelectionMap sASm = new SingleAlgorithmSelectionMap(alg1);
			
		boolean thrown = false;
		AlgorithmSelectionSystem sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sASm).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			thrown = true;
		}
		assertTrue(thrown);
	
		//Test normal
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		sASm = new SingleAlgorithmSelectionMap(alg1);
		
		algorithms2.put(alg1, extractor);
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sASm).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
		//Check if all was initialised correctly
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys2.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		assertTrue(sys2.getFeatureValueExtractorMap().isEmpty());
		assertTrue(sys2.getPerformanceExtractors().size() == 1);
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg1));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys2.getSelectionMap().equals(sASm));
		
		//Testing deep copies
		AlgorithmRunDatabase aRDb = sys2.getAlgorithmRunDatabase();
		aRDb.addAlgorithmRun(run);
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		
		FeatureValueDatabase fvDb = sys2.getFeatureValueDatabase();
		fvDb.addRecord(featureValueRecord);
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());

		
	}
	@Test
	public void testAlgorithmSelectionSystemWithFeatures() {

		HashMap<Feature, FeatureValueExtractor> features2 = new HashMap<Feature, FeatureValueExtractor>();
		HashMap<ExecutableAlgorithm, PerformanceExtractor> algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
			
		boolean thrown = false;
		AlgorithmSelectionSystem sys2 = null;

		//Test: only 1 of the 2 algorithms added
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		features2.put(f1, featExtractor);
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			thrown = true;
		}
		assertTrue(thrown);
	
	
		//Test: feature not added
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);


		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
		//Test normal
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);
		features2.put(f1, featExtractor);
		
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}

		//Check if all was initialised correctly
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys2.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		assertTrue(sys2.getFeatureValueExtractorMap().size() == 1);
		assertTrue(sys2.getFeatureValueExtractorMap().containsKey(f1));
		assertTrue(sys2.getPerformanceExtractors().size() == 2);
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg1));
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg2));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys2.getSelectionMap().equals(sm));
	}

	
	@Test
	public void testAlgorithmSelectionSystemWithInitDatabases() {
		//First all the same tests as for the other constructor
		AlgorithmRunDatabase initAlgRunDb = new AlgorithmRunDatabase();
		FeatureValueDatabase initFeatureValueDb = new FeatureValueDatabase();
		
		//Empty init feat, 1 alg tests
		
		//Test: alg required by selection map not specified
		HashMap<Feature, FeatureValueExtractor> features2 = new HashMap<Feature, FeatureValueExtractor>();
		HashMap<ExecutableAlgorithm, PerformanceExtractor> algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		SingleAlgorithmSelectionMap sASm = new SingleAlgorithmSelectionMap(alg1);
		
		boolean thrown = false;
		
		
		AlgorithmSelectionSystem sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sASm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
		//Test normal
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		sASm = new SingleAlgorithmSelectionMap(alg1);
		
		algorithms2.put(alg1, extractor);
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sASm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
		
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys2.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		assertTrue(sys2.getFeatureValueExtractorMap().isEmpty());
		assertTrue(sys2.getPerformanceExtractors().size() == 1);
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg1));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys2.getSelectionMap().equals(sASm));
		
		//Selection with features
		
		//Test: only 1 of the 2 algorithms added
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		features2.put(f1, featExtractor);
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir					
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			thrown = true;
		}
		assertTrue(thrown);

		//Test: feature not added
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);

		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
		//Test normal
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);
		features2.put(f1, featExtractor);
		
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}

		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys2.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		assertTrue(sys2.getFeatureValueExtractorMap().size() == 1);
		assertTrue(sys2.getFeatureValueExtractorMap().containsKey(f1));
		assertTrue(sys2.getPerformanceExtractors().size() == 2);
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg1));
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg2));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys2.getSelectionMap().equals(sm));
	
		
		//Additional tests
		
		//Test that an initial feature value database with records for features for which no extractor
		//is available can indeed be used to initialise the system
		initAlgRunDb = new AlgorithmRunDatabase();
		initFeatureValueDb = new FeatureValueDatabase();
		Feature f2 = new Feature("f2");
		FeatureValueRecord rec = new FeatureValueRecord(inst, f2, 10);
		initFeatureValueDb.addRecord(rec);
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);
		features2.put(f1, featExtractor);		
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}

		assertTrue(sys2.getFeatureValueDatabase().equals(initFeatureValueDb));
		
		
		
		//Test that an initial algorithm run database with records for algorithms for which no extractor
		//is available can indeed be used to initialise the system
		initAlgRunDb = new AlgorithmRunDatabase();
		initFeatureValueDb = new FeatureValueDatabase();
		AlgorithmRun initRun = null;
		try {
			initRun = new AlgorithmRun(new ExecutableAlgorithm.Builder("notyetexistant", exec1).build(), inst, new MinimisationPerformance(10));
		} catch (IllegalArgumentException e1) {
			fail();
		} catch (FileNotFoundException e1) {
			fail();
		};
		initAlgRunDb.addAlgorithmRun(initRun);
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);
		features2.put(f1, featExtractor);		
		thrown = false;
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
		assertTrue(sys2.getAlgorithmRunDatabase().equals(initAlgRunDb));

		
	
		
		//Normal functioning
		initAlgRunDb = new AlgorithmRunDatabase();
		initFeatureValueDb = new FeatureValueDatabase();
		initRun = new AlgorithmRun(alg1, inst, new MinimisationPerformance(10));;
		AlgorithmRun initRun2 = new AlgorithmRun(alg2, inst, new MinimisationPerformance(10));;
		initAlgRunDb.addAlgorithmRun(initRun);
		initAlgRunDb.addAlgorithmRun(initRun2);

		rec = new FeatureValueRecord(inst, f1, 10);
		initFeatureValueDb.addRecord(rec);
		features2 = new HashMap<Feature, FeatureValueExtractor>();
		algorithms2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();		
		algorithms2.put(alg2, extractor);
		algorithms2.put(alg1, extractor);
		features2.put(f1, featExtractor);		
		sys2 = null;
		try {
			sys2 = new AlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractors(algorithms2).featureValueExtractorMap(features2).selectionMap(sm).algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}

	

		assertTrue(sys2.getAlgorithmRunDatabase().equals(initAlgRunDb));
		assertTrue(sys2.getFeatureValueDatabase().equals(initFeatureValueDb));
		assertTrue(sys2.getFeatureValueExtractorMap().size() == 1);
		assertTrue(sys2.getFeatureValueExtractorMap().containsKey(f1));
		assertTrue(sys2.getPerformanceExtractors().size() == 2);
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg1));
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg2));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys2.getSelectionMap().equals(sm));

		


	}

	
	@Test
	public void testGetSelectionmap() {
		assertTrue(sys.getSelectionMap().equals(sm));
	}

	@Test
	public void testGetFeatureValueExtractorFor() {
		try {
			assertTrue(sys.getFeatureValueExtractorFor(f1).equals(featExtractor));
		} catch (FeatureNotFoundException e) {
			fail();
		}
		
		
		Feature f2 = new Feature("f2");
		boolean thrown = false;
		
		try {
			sys.getFeatureValueExtractorFor(f2);
		} catch (FeatureNotFoundException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
	}

	@Test
	public void testAddFeature() {
		Feature f2 = new Feature("f2");
		ToyFeatureValueExtractor ex2 = new ToyFeatureValueExtractor();
		assertTrue(sys.getFeatureValueExtractorMap().size() == 1);
		sys.addFeature(f2,ex2);
		assertTrue(sys.getFeatureValueExtractorMap().size() == 2);
		try {
			assertTrue(sys.getFeatureValueExtractorFor(f2).equals(ex2));
		} catch (FeatureNotFoundException e) {
			fail();
		}
	}

	@Test
	public void testAddFeatureValueRecord() {
		
		//Adding a new record: standard
		FeatureValueRecord rec = new FeatureValueRecord(inst, f1, 10);
		assertFalse(sys.getFeatureValueDatabase().getFeatureValueRecords().contains(rec));
		sys.addFeatureValueRecord(rec);
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().contains(rec));

	
	}

	@Test
	public void testGetFeatureValueDatabase(){
		//Deep copy test
		FeatureValueDatabase fDb = sys.getFeatureValueDatabase();
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		fDb.addRecord(new FeatureValueRecord(inst, f1, 5));
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());

	}
	
	@Test
	public void testProcessInstance() {
		
		
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());

		try {
			sys.processInstance(inst, "tempResFile", "tempErrFile");
		}  catch (InterruptedException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (InvalidFileForFeatureValueExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
		//Test that the 1 feature was extracted and added to the feature value database
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().size()==1);
		//Test that the algorithm run was added to the algorithm run database
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().size() ==1);
	
		

		
	}
	
	@Test
	public void testGetAvailableFeatures(){
		assertTrue(sys.getAvailableFeatures().contains(f1));
		assertTrue(sys.getAvailableFeatures().size() == 1);
;	}

}
