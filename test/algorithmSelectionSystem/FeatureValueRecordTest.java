package algorithmSelectionSystem;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import performanceDatabase.Instance;

public class FeatureValueRecordTest  {
	Instance i1;
	Feature f1;
	double value1;
	FeatureValueRecord rec1;
	
	@Before
	public void setUp() throws Exception {
		i1 = new Instance("i1");
		f1 = new Feature ("f1");
		value1 = 10;
		rec1 = new FeatureValueRecord(i1,f1, value1);
	}

	@Test
	public void testGetFeature() {
		assertTrue(f1.equals(rec1.getFeature()));
	}

	@Test
	public void testGetInstance() {
		assertTrue(i1.equals(rec1.getInstance()));
	}

	@Test
	public void testGetValue() {
		assertTrue(value1 == rec1.getValue());
	}

}
