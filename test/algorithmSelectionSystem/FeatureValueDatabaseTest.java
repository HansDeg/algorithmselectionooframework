package algorithmSelectionSystem;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import performanceDatabase.Instance;
public class FeatureValueDatabaseTest {

	Feature f1;
	Instance i1;
	double value1;
	
	FeatureValueRecord r1;
	FeatureValueDatabase db;
	
	@Before
	public void setUp() throws Exception {
		f1 = new Feature("f1");
		i1 = new Instance("i1");
		value1 = 1;
		
		r1 = new FeatureValueRecord(i1,f1,value1);
		HashSet<FeatureValueRecord> records = new HashSet<FeatureValueRecord>();
		records.add(r1);
		db = new FeatureValueDatabase(records);
	}

	@Test
	public void testFeatureValueDatabaseHashSetOfFeatureValueRecord() {
		HashSet<FeatureValueRecord> records = new HashSet<FeatureValueRecord>();
		records.add(r1);
		FeatureValueDatabase database = new FeatureValueDatabase(records);
		
		assertTrue(database.getFeatureValueRecords().contains(r1));
		assertTrue(database.getFeatureValueRecords().size() == 1);
		
		FeatureValueRecord r2 = new FeatureValueRecord(i1,f1,45);
		records.add(r2);
		assertTrue(database.getFeatureValueRecords().size() == 1); //Test deep copy when constructing
		
	}

	@Test
	public void testFeatureValueDatabase() {
		FeatureValueDatabase database = new FeatureValueDatabase();
		assertTrue(database.getFeatureValueRecords().equals(new HashSet<FeatureValueRecord>()));
	}

	@Test
	public void testGetFeatureValueRecords() {
		HashSet<FeatureValueRecord> recs = db.getFeatureValueRecords();
		recs.add(new FeatureValueRecord(i1,f1,45));
		assertFalse(recs.equals(db.getFeatureValueRecords())); //Check deep copy was made
	}

	@Test
	public void testAddRecord() {
		FeatureValueRecord newR = new FeatureValueRecord(i1,f1,4);
		int initSize = db.getFeatureValueRecords().size();
		db.addRecord(newR);
		assertTrue(db.getFeatureValueRecords().size() == initSize+1);
		
	}
	
	@Test
	public void testGetAllFeatures(){
		assertTrue(db.getAllFeatures().size() == 1);
		assertTrue(db.getAllFeatures().contains(f1));
	}
	
	@Test
	public void testGetRecordsOfInstance(){
		Instance i2 = new Instance("i2");
		Feature f2 = new Feature("f2");
		FeatureValueRecord r2 = new FeatureValueRecord(i2, f1, 3);
		FeatureValueRecord r3 = new FeatureValueRecord(i2, f2, 5);
		FeatureValueRecord r4 = new FeatureValueRecord(i1, f2, 7);

		db.addRecord(r2);
		db.addRecord(r3);
		db.addRecord(r4);
		
		HashSet<FeatureValueRecord> testSet = db.getRecordsWithInstance(i2);
		assertTrue(testSet.size() == 2);
		assertTrue(testSet.contains(r2));
		assertTrue(testSet.contains(r3));
	}
	
	@Test
	public void testGetRecordOfFeatureAndInstance(){
		Instance i2 = new Instance("i2");
		Feature f2 = new Feature("f2");
		
		//Test: null returned for non-existant combinations of features and instances
		//Both non-existant
		FeatureValueRecord res = db.getRecord(i2, f2);
		assertNull(res);
		res = db.getRecord(i2, f1);
		assertNull(res);
		res = db.getRecord(i1, f2);
		assertNull(res);
		res = db.getRecord(i1, f1);
		assertTrue(res.equals(r1));


		
	}

	
	@Test
	public void testEqualsAndHashCode(){
		FeatureValueDatabase db1 = new FeatureValueDatabase();
		FeatureValueDatabase db2 = new FeatureValueDatabase();

		db1.addRecord(r1);
		db2.addRecord(r1);
		
		assertTrue(db1.equals(db2));
		assertTrue(db1.hashCode() == db2.hashCode());
		assertTrue(db1.getFeatureValueRecords().hashCode() == db1.hashCode());
		
		db1.addRecord(new FeatureValueRecord(i1, new Feature("some feature"), 15));
		assertFalse(db1.equals(db2));
		assertFalse(db1.hashCode() == db2.hashCode());
		assertTrue(db1.getFeatureValueRecords().hashCode() == db1.hashCode());
		assertTrue(db2.getFeatureValueRecords().hashCode() == db2.hashCode());

		
	}
}



