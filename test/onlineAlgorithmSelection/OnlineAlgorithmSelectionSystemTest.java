package onlineAlgorithmSelection;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyExamplePerformanceExtractor;
import ToyExample.ToyFeatureValueExtractor;
import ToyExample.ToyPerformanceTestFileLocations;
import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import algorithmSelectionSystem.FeatureValueExtractor;
import algorithmSelectionSystem.FeatureValueRecord;
import algorithmSelectionSystem.InvalidFileForFeatureValueExtractionException;
import algorithmSelectionSystem.InvalidSelectionMapException;
import algorithmSelectionSystem.SelectionMap;
import executableAlgorithmSystem.ExecutableAlgorithm;
import executableAlgorithmSystem.InvalidFileForPerformanceExtractionException;
import executableAlgorithmSystem.PerformanceExtractor;
import executableAlgorithmSystem.RunnableInstance;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import performanceDatabase.Performance;
import standardPerformances.MinimisationPerformance;
import standardSelectionMapInitialisationStrategies.PickObservedSbsStrategy;
import standardSelectionMaps.BinarySelectionMap;

public class OnlineAlgorithmSelectionSystemTest{
	ExecutableAlgorithm alg1, alg2;
	File exec1,exec2;
	String execLocation1 = ToyPerformanceTestFileLocations.EXEC_LINEAR;
	String execLocation2 = ToyPerformanceTestFileLocations.EXEC_CONSTANT;
	ToyExamplePerformanceExtractor extractor;

	
	RunnableInstance inst;
	File instFile;
	String instFileLocation = ToyPerformanceTestFileLocations.TOY_INST; 

	BinarySelectionMap sm;

	OnlineAlgorithmSelectionSystem sys;
	String probName = "toy problem";
	
	String resDirLoc = ToyPerformanceTestFileLocations.RES_DIRECTORY;
	File resDir;
	
	Performance runPerf;
	AlgorithmRun run;
	
	ToyFeatureValueExtractor featExtractor;
	Feature f1;
	
	HashMap<ExecutableAlgorithm, PerformanceExtractor> algorithms;
	HashMap<Feature, FeatureValueExtractor> features;
	
	double smCrossoverValue = 100;

	FeatureValueRecord featureValueRecord;
	
	PickObservedSbsStrategy strategy;


	@Before
	public void setUp() throws Exception {
		assertTrue(new File(execLocation1).exists());
		assertTrue(new File(execLocation2).exists());
		assertTrue(new File(instFileLocation).exists());
		assertTrue(new File(resDirLoc).exists());
		
		

		ArrayList<String> prefixes = new ArrayList<String>();
		prefixes.add("java");
		prefixes.add("-jar");
		
		
		exec1 = new File(execLocation1);
		exec2 = new File(execLocation2);
		alg1 = new ExecutableAlgorithm.Builder("alg1", exec1)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();
		alg2 = new ExecutableAlgorithm.Builder("alg2", exec2)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();

		 
		instFile = new File(instFileLocation);
		inst = new RunnableInstance("i1", instFile);
		
		extractor = new ToyExamplePerformanceExtractor();
		
		resDir = new File(resDirLoc);
		
		
		f1 = new Feature("problem size");
		sm = new BinarySelectionMap(alg1, alg2, f1, smCrossoverValue);

		algorithms = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		algorithms.put(alg1,  extractor);
		algorithms.put(alg2, extractor);
		
		featExtractor = new ToyFeatureValueExtractor();
		features = new HashMap<Feature, FeatureValueExtractor>();
		features.put(f1, featExtractor);
		
		
		
		runPerf = new MinimisationPerformance(10);

		run = new AlgorithmRun(alg1, inst, runPerf);

		featureValueRecord = new FeatureValueRecord(inst, f1, featExtractor.extractValue(inst) );		

		
		strategy = new PickObservedSbsStrategy();
		
		sys = new OnlineAlgorithmSelectionSystem.Builder(probName, resDir).performanceExtractorMap(algorithms).featureValueExtractorMap(features).strategy(strategy).build();

	}


	@Test
	public void testOnlineAlgorithmSelectionSystem() {
		//Test series 1: no algorithmRunDatabase nor a FeatureValueDatabase
		
		
		//1.a No selection strategy was specified
		boolean thrown = false;
		try{
			sys = new OnlineAlgorithmSelectionSystem.Builder(probName, resDir).
					performanceExtractorMap(algorithms).build();
		}
		catch(IllegalArgumentException e){
			thrown = true;
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
		assertTrue(thrown);
	
		//1.b normal initialisation without initial data
		try{
			sys = new OnlineAlgorithmSelectionSystem.Builder(probName, resDir).
					performanceExtractorMap(algorithms).strategy(strategy).build();
		}
		catch(IllegalArgumentException e){
			fail();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		assertTrue(sys.getFeatureValueExtractorMap().isEmpty());
		assertTrue(sys.getPerformanceExtractors().size() == 2);
		assertTrue(sys.getPerformanceExtractors().containsKey(alg1));
		assertTrue(sys.getPerformanceExtractors().containsKey(alg2));
		assertTrue(sys.getProblemName().equals(probName));
		assertTrue(sys.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys.getStrategy().equals(strategy));
		assertTrue(sys.getSelectionMap() != null);

		//Testing deep copies
		try {
			algorithms.put(new ExecutableAlgorithm.Builder("aNew", exec1).build(), extractor);
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		}
		assertTrue(sys.getAvailableAlgorithms().size() == 2);
		
		AlgorithmRunDatabase aRDb = sys.getAlgorithmRunDatabase();
		aRDb.addAlgorithmRun(run);
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		
	}
	
	@Test
	public void testAlgorithmSelectionSystemWithInitDatabases() {
		//First all the same tests as for the other constructor
		AlgorithmRunDatabase initAlgRunDb = new AlgorithmRunDatabase();
		FeatureValueDatabase initFeatureValueDb = new FeatureValueDatabase();
		
		//Normal functioning
		initAlgRunDb = new AlgorithmRunDatabase();
		initFeatureValueDb = new FeatureValueDatabase();
		AlgorithmRun initRun = new AlgorithmRun(alg1, inst, new MinimisationPerformance(10));;
		AlgorithmRun initRun2 = new AlgorithmRun(alg2, inst, new MinimisationPerformance(10));;
		initAlgRunDb.addAlgorithmRun(initRun);
		initAlgRunDb.addAlgorithmRun(initRun2);

		FeatureValueRecord rec = new FeatureValueRecord(inst, f1, 10);
		initFeatureValueDb.addRecord(rec);
		sys = null;
		try {
			sys = new OnlineAlgorithmSelectionSystem.Builder(probName, resDir
				).performanceExtractorMap(algorithms).featureValueExtractorMap(features).strategy(strategy)
					.algorithmRunDatabase(initAlgRunDb).featureValueDatabase(initFeatureValueDb).build();
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		}
	
		
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().size() == 2);
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().contains(initRun));
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().contains(initRun2));
		assertTrue(sys.getAvailableAlgorithms().contains(alg1));
		assertTrue(sys.getAvailableAlgorithms().contains(alg2));
		assertTrue(sys.getAvailableAlgorithms().size() == 2);
		assertTrue(sys.getAvailableFeatures().contains(f1));
		assertTrue(sys.getAvailableFeatures().size() == 1);
		assertTrue(sys.getProblemName().equals(probName));
		assertTrue(sys.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		assertTrue(sys.getStrategy().equals(strategy));
		assertTrue(sys.getSelectionMap() != null);
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().contains(rec));
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().size() == 1);

		//Asserting deep copies
		try {
			algorithms.put(new ExecutableAlgorithm.Builder("aNew", exec1).build(), extractor);
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		}
		assertTrue(sys.getAvailableAlgorithms().size() == 2);
		
		AlgorithmRunDatabase aRDb = sys.getAlgorithmRunDatabase();
		aRDb.addAlgorithmRun(run);
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().size()==2);
		

	}

		
	@Test
	public void testGetStrategy() {
		assertTrue(sys.getStrategy().equals(strategy));
	}
	
	
	
	@Test
	public void testProcessInstance() {
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().isEmpty());
		assertTrue(sys.getFeatureValueDatabase().getFeatureValueRecords().isEmpty());
		SelectionMap beforeSm = sys.getSelectionMap();
		try {
			sys.processInstance(inst, "testTempResFile", "tempErrFile");
		}  catch (InterruptedException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (InvalidFileForFeatureValueExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		} catch (InvalidSelectionMapException e) {
			fail();
		} 
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().size() ==1);
		assertFalse(beforeSm.equals(sys.getSelectionMap())); //Selection map has changed
		
		
		
	}

}
