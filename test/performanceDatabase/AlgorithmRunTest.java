package performanceDatabase;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import standardPerformances.MaximisationPerformance;

public class AlgorithmRunTest {

	Algorithm alg;
	Instance inst;
	String id;
	Performance perf;  //Note: MaximisationPerformance is used as concrete implementation of Performance
	AlgorithmRun run;

	@Before
	public void setUp() throws Exception {
		alg = new Algorithm("a1");
		inst = new Instance("i1");
		perf = new MaximisationPerformance(10);
		id = inst.getName();
		run = new AlgorithmRun(alg, inst, perf);
	}

	@Test
	public void testAlgorithmRun() {
		  boolean thrown = false;
		  @SuppressWarnings("unused")
		  AlgorithmRun algRun = null;
		  
		  try {
			  algRun = new AlgorithmRun(null, inst, perf);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertTrue(thrown);

		  thrown = false;

		  try {
			  algRun = new AlgorithmRun(alg, null, perf);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertTrue(thrown);

		  thrown = false;

		  try {
			  algRun = new AlgorithmRun(alg, inst, null);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertTrue(thrown);

		  thrown = false;

		  thrown=false;
		  try {
			  algRun = new AlgorithmRun(alg, inst, perf);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertFalse(thrown);

		  thrown = false;

		  //Equalities of members are tested in the gettetsts
	}

	@Test
	public void testGetAlgorithm() {
		assertTrue(alg.equals(run.getAlgorithm()));
	}

	@Test
	public void testGetInstance() {
		assertTrue(inst.equals(run.getInstance()));
	}

	@Test
	public void testGetPerformance() {
		assertTrue(perf.equals(run.getPerformance()));
	}

	

}
