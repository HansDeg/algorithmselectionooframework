package performanceDatabase;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import standardPerformances.MaximisationPerformance;

/**
 * 
 * @author Hans Degroote
 * 
 * Uses the MaximisationPerformance implementation of Performance for the tests
 * In Maximisationperformance, one performance is better than an other if its objective value is larger
 *
 */
public class PerformanceTest {

	MaximisationPerformance perf1;
	MaximisationPerformance perf2;
	MaximisationPerformance perf3; 
	
	double objValue1 = 10;
	double objValue2 = 10;
	double objValue3 = 25;
	
	double perfValue1 = 10; //Depends on MaximisationPerformance implementation
	
	@Before
	public void setUp() throws Exception {
		perf1 = new MaximisationPerformance(objValue1);
		perf2 = new MaximisationPerformance(objValue2);
		perf3 = new MaximisationPerformance(objValue3);

	}

	@Test
	public void testHashCode() {
		assertTrue(perf1.hashCode() == perf2.hashCode());
		assertTrue(perf2.hashCode() == perf1.hashCode());
	}

	@Test
	public void testCompareTo() {
		assertTrue(perf1.compareTo(perf2) == 0);
		assertTrue(perf2.compareTo(perf1) == 0);
		assertTrue(perf1.compareTo(perf3) < 0);
		assertTrue(perf3.compareTo(perf1) > 0);
	}

	@Test
	public void testGetValue() {
		assertTrue(perf1.getValue() == perfValue1); 
	}

	@Test
	public void testEqualsObject() {
		assertTrue(perf1.equals(perf2));
		assertTrue(perf2.equals(perf1));
		assertFalse(perf1.equals(perf3));
		assertFalse(perf3.equals(perf1));
	}
}

