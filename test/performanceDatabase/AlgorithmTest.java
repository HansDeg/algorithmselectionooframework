package performanceDatabase;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class AlgorithmTest {

	Algorithm alg1;
	Algorithm alg2SameName;
	Algorithm alg3DiffName;
	
	String algorithmName1 = "sameName";
	String algorithmName2 = "sameName";
	String algorithmName3 = "otherName";

	@Before
	public void setUp() throws Exception {
		alg1 = new Algorithm(algorithmName1);
		alg2SameName = new Algorithm(algorithmName2);
		alg3DiffName = new Algorithm(algorithmName3);
		
	}

	@Test
	public void testAlgorithm() {
		  boolean thrown = false;

		  try {
			  new Algorithm(null); //Verifies that illegalargumentexception is indeed thrown for null name
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertTrue(thrown);
		  
		  String name = "alg1";
		  Algorithm a = null;
		  try {
			  a = new Algorithm(name); //Verifies that illegalargumentexception is indeed thrown for null name
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }
		  assertTrue(a.getName() == "alg1");

	}
	
	@Test
	public void testHashCode() {
		assertTrue(alg1.hashCode() == alg2SameName.hashCode());
		assertTrue(alg2SameName.hashCode() == alg1.hashCode());
	}

	@Test
	public void testGetName() {
		assertTrue(alg1.getName().equals(algorithmName1));
	}

	@Test
	public void testEqualsObject() {
		assertTrue(alg1.equals(alg2SameName));
		assertTrue(alg2SameName.equals(alg1));
		assertFalse(alg1.equals(algorithmName3));
		assertFalse(algorithmName3.equals(alg1));
	}

	
	

}
