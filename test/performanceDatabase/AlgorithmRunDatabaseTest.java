package performanceDatabase;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import standardPerformances.MaximisationPerformance;
import standardPerformances.MinimisationPerformance;

public class AlgorithmRunDatabaseTest{

	Algorithm alg;
	Instance inst;
	Performance perf;
	AlgorithmRun run;
	Set<AlgorithmRun> algorithmRuns;
	AlgorithmRunDatabase db;

	@Before
	public void setUp() throws Exception {
		alg = new Algorithm("a1");
		inst = new Instance("i1");
		perf = new MaximisationPerformance(10);
		
		run = new AlgorithmRun(alg, inst, perf);
		algorithmRuns = new HashSet<AlgorithmRun>();
		algorithmRuns.add(run);
		db = new AlgorithmRunDatabase(algorithmRuns);
	}

	@Test
	public void testAlgorithmRunDatabaseHashSetOfAlgorithmRun() {
		HashSet<AlgorithmRun> testSet = new HashSet<AlgorithmRun>();
		testSet.add(run);
		AlgorithmRunDatabase testDb = new AlgorithmRunDatabase(testSet);
		assertTrue(testDb.getAlgorithmRuns().equals(testSet));
		
		//Test that a deep copy was made in the constructor
		Algorithm newA = new Algorithm("a2");
		Instance newI = new Instance("i2");
		Performance newP = new MaximisationPerformance(20);
		AlgorithmRun newR = new AlgorithmRun(newA, newI, newP);		
		testSet.add(newR);
		assertFalse(testDb.getAlgorithmRuns().equals(testSet));
	}

	@Test
	public void testGetAlgorithmRuns() {
		assertTrue(db.getAlgorithmRuns().equals(algorithmRuns));
		
		//Test that a deep copy is returned
		HashSet<AlgorithmRun> algRuns = db.getAlgorithmRuns();
		assertTrue(db.getAlgorithmRuns().equals(algRuns));
		Algorithm newA = new Algorithm("a2");
		Instance newI = new Instance("i2");
		Performance newP = new MaximisationPerformance(20);
		AlgorithmRun newR = new AlgorithmRun(newA, newI, newP);
		algRuns.add(newR);
		assertFalse(db.getAlgorithmRuns().equals(algRuns));

	}

	@Test
	public void testAddAlgorithmRun() {
		Algorithm newA = new Algorithm("a2");
		Instance newI = new Instance("i2");
		Performance newP = new MaximisationPerformance(20);
		AlgorithmRun newR = new AlgorithmRun(newA, newI, newP);
		
		assertFalse(db.getAlgorithmRuns().contains(newR));
		db.addAlgorithmRun(newR);
		assertTrue(db.getAlgorithmRuns().contains(newR));
	}

	@Test
	public void testGetRunsOfAlgOnInst(){
		HashSet<AlgorithmRun> origRuns = db.getRunsOfAlgOnInst(alg, inst);
		assertTrue(origRuns.contains(run));
		assertTrue(origRuns.size() == 1);
		
		
		AlgorithmRun newRun = new AlgorithmRun(alg, inst, new MaximisationPerformance(50));
		db.addAlgorithmRun(newRun);
		
		HashSet<AlgorithmRun> updatedRuns = db.getRunsOfAlgOnInst(alg, inst);
		assertTrue(updatedRuns.contains(run));
		assertTrue(updatedRuns.contains(newRun));
		assertTrue(updatedRuns.size() == 2);
	}
	
	@Test
	public void testGetAllAlgorithms(){
		assertTrue(db.getAllAlgorithms().size() == 1);
		
		//Adding a run for  a new algorithm
		Instance i2 = new Instance("i2");
		Algorithm a2 = new Algorithm("a2");
		db.addAlgorithmRun(new AlgorithmRun(alg, i2, new MaximisationPerformance(15)));
		assertTrue(db.getAllAlgorithms().size() == 1);		
		db.addAlgorithmRun(new AlgorithmRun(a2, i2, new MaximisationPerformance(10)));
		assertTrue(db.getAllAlgorithms().size() == 2);
	}
	
	@Test
	public void testGetAllInstances(){
		assertTrue(db.getAllInstances().size() == 1);
		
		//Adding a run for a new instance
		Instance i2 = new Instance("i2");
		Algorithm a2 = new Algorithm("a2");
		db.addAlgorithmRun(new AlgorithmRun(a2, inst, new MaximisationPerformance(10)));
		assertTrue(db.getAllInstances().size() == 1);
		
		db.addAlgorithmRun(new AlgorithmRun(alg, i2, new MaximisationPerformance(15)));
		assertTrue(db.getAllInstances().size() == 2);
		

	}
	
	@Test
	public void testGetAllRunsOfAlgorithm(){
		Instance i2 = new Instance("i2");
		AlgorithmRun r1 = new AlgorithmRun(alg, i2, new MaximisationPerformance(15));
		AlgorithmRun r2 = new AlgorithmRun(alg, inst, new MaximisationPerformance(15));
		AlgorithmRun r3 = new AlgorithmRun(new Algorithm("tempAlg"), i2, new MaximisationPerformance(15));
		db.addAlgorithmRun(r1);
		db.addAlgorithmRun(r2);
		db.addAlgorithmRun(r3);
		
		HashSet<AlgorithmRun> runs = db.getAllRunsOfAlgorithm(alg);
		assertTrue(runs.size() == 3); //1 from setup, 2 here
		assertTrue(runs.contains(r1));
		assertTrue(runs.contains(r2));
		assertTrue(runs.contains(run));
		assertFalse(runs.contains(r3));
	}
	
	@Test
	public void testGetAllRunsOnInstance(){
		Instance i2 = new Instance("i2");
		Algorithm a2 = new Algorithm("a2");
		AlgorithmRun r2 = new AlgorithmRun(a2, i2, new MaximisationPerformance(12));
		AlgorithmRun r3 = new AlgorithmRun(a2, inst, new MaximisationPerformance(12));
		db.addAlgorithmRun(r2);
		db.addAlgorithmRun(r3);
		
		HashSet<AlgorithmRun> runsSet = db.getAllRunsOnInstance(inst);
		assertTrue(runsSet.size() == 2);
		assertTrue(runsSet.contains(run));
		assertTrue(runsSet.contains(r3));
		
	}
	
	@Test
	public void testEqualsAndHashCode(){
		AlgorithmRunDatabase db1 = new AlgorithmRunDatabase();
		AlgorithmRunDatabase db2 = new AlgorithmRunDatabase();

		db1.addAlgorithmRun(run);
		db2.addAlgorithmRun(run);
		
		assertTrue(db1.equals(db2));
		assertTrue(db1.hashCode() == db2.hashCode());
		assertTrue(db1.getAlgorithmRuns().hashCode() == db1.hashCode());
		
		db1.addAlgorithmRun(new AlgorithmRun(alg,inst, new MinimisationPerformance(50)));
		assertFalse(db1.equals(db2));
		assertFalse(db1.hashCode() == db2.hashCode());
		assertTrue(db1.getAlgorithmRuns().hashCode() == db1.hashCode());
		assertTrue(db2.getAlgorithmRuns().hashCode() == db2.hashCode());

		
	}
	
	

}