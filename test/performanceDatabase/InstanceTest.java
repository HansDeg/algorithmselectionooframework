package performanceDatabase;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class InstanceTest {


	Instance inst1;
	Instance inst2;
	Instance inst3;
	
	String instanceName1 = "sameName";
	String instanceName2 = "sameName";
	String instanceName3 = "otherName";
	
	@Before
	public void setUp() throws Exception {
		inst1 = new Instance(instanceName1);
		inst2 = new Instance(instanceName2);
		inst3 = new Instance(instanceName3);
	}

	@Test 
	public void testInstance(){
		  boolean thrown = false;

		  try {
			  new Instance(null); //Verifies that illegalargumentexception is indeed thrown for null name
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }

		  assertTrue(thrown);
		  
		  String name = "testName";
		  Instance i1 = new Instance(name);
		  assertTrue(i1.getName() == name);

	}
	
	@Test
	public void testHashCode() {
		assertTrue(inst1.hashCode() == inst2.hashCode());
		assertTrue(inst2.hashCode() == inst1.hashCode());
	}

	@Test
	public void testGetName() {
		assertTrue(inst1.getName().equals(instanceName1));
	}

	@Test
	public void testEqualsObject() {
		assertTrue(inst1.equals(inst2));
		assertTrue(inst2.equals(inst1));
		assertFalse(inst1.equals(inst3));
		assertFalse(inst3.equals(inst1));
	}


}
