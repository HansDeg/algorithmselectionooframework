package ToyExample;

import java.io.File;

public class ToyPerformanceTestFileLocations {
	public static String TOY_INST = ToyExampleDirectoryLocations.TEST_FILES_DIRECTORY + "toyInst1.txt";
	public static String TOY_RES = ToyExampleDirectoryLocations.TEST_FILES_DIRECTORY + "toyResFile.txt";
	public static String NONSENSE_FILE = ToyExampleDirectoryLocations.TEST_FILES_DIRECTORY + "nonsenseFile.txt";
	public static String EXEC_CONSTANT = ToyExampleDirectoryLocations.EXECUTABLES_DIRECTORY + "constAlg.jar";
	public static String EXEC_LINEAR = ToyExampleDirectoryLocations.EXECUTABLES_DIRECTORY + "linearAlg.jar";
	public static String RES_DIRECTORY = ToyExampleDirectoryLocations.RESULTS_DIRECTORY + "unitTestTempFiles" +
			File.separatorChar;
	public static String EXEC_PYTHON_LINEAR = ToyExampleDirectoryLocations.EXECUTABLES_DIRECTORY + "linearAlgPython.py";
	public static final String EXEC_R_CONSTANT =ToyExampleDirectoryLocations.EXECUTABLES_DIRECTORY + "RLinearAlg.R";
	public static String TOY_ERR = ToyExampleDirectoryLocations.TEST_FILES_DIRECTORY + "toyErrFile.txt";
}
