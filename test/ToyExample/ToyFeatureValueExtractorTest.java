package ToyExample;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import algorithmSelectionSystem.InvalidFileForFeatureValueExtractionException;
import executableAlgorithmSystem.RunnableInstance;

public class ToyFeatureValueExtractorTest {

	@Test
	public void test() {
		
		
		ToyFeatureValueExtractor extractor = new ToyFeatureValueExtractor();
		//Null argument
		boolean thrown = true;
		try {
			extractor.extractValue(null);
		} catch (InvalidFileForFeatureValueExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		} catch(IllegalArgumentException e){
			thrown = true;
		}
		assertTrue(thrown);
		
		//Wrong kind of file was added to the instance
		RunnableInstance iNonsense = null;
		try {
			iNonsense = new RunnableInstance("iNonsense", new File(ToyPerformanceTestFileLocations.NONSENSE_FILE));
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		}
		thrown = true;
		try {
			extractor.extractValue(iNonsense);
		} catch (InvalidFileForFeatureValueExtractionException e) {
			thrown=true;
		} catch (IOException e) {
			fail();
		} catch(IllegalArgumentException e){
			fail();
		}
		assertTrue(thrown);

		
		
		
		//Correct use
		RunnableInstance i1 = null;
		try {
			i1 = new RunnableInstance("i1", new File(ToyPerformanceTestFileLocations.TOY_INST));
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		}
		

		try {
			extractor.extractValue(i1);
		} catch (InvalidFileForFeatureValueExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

}
