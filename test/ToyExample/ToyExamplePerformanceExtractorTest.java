package ToyExample;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import executableAlgorithmSystem.InvalidFileForPerformanceExtractionException;
import performanceDatabase.Performance;

public class ToyExamplePerformanceExtractorTest {

	@Test
	public void test() {
		ToyExamplePerformanceExtractor extractor = new ToyExamplePerformanceExtractor();
		
		boolean thrown = false;
		try {
			extractor.extractPerformance(null);
		} catch (IllegalArgumentException e) {
			thrown = true;
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
		assertTrue(thrown);
		
		thrown = false;
		try {
			extractor.extractPerformance(new File("nonExistant.blablaz"));
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			thrown = true;
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
		assertTrue(thrown);
		
		//Too short wrong file
		File invalidFile = new File(ToyExampleDirectoryLocations.TEST_FILES_DIRECTORY + "1LineFile.txt");
		thrown = false;
		try {
			extractor.extractPerformance(invalidFile);
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			thrown = true;
		} catch (IOException e) {
			fail();
		}
		assertTrue(thrown);
		
		//Completely wrong file of sufficient length
		File tooLongFile = new File(ToyPerformanceTestFileLocations.NONSENSE_FILE);
		thrown = false;
		try {
			extractor.extractPerformance(tooLongFile);
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			thrown = true;
		} catch (IOException e) {
			fail();
		}
		assertTrue(thrown);

		
		//Correct file
		File correctFile = new File(ToyPerformanceTestFileLocations.TOY_RES);
		Performance resPerf = null;
		double correctValue = -50;
		try {
			resPerf = extractor.extractPerformance(correctFile);
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
		assertTrue(resPerf.getValue() == correctValue); //Minimisation performance: value = -obj value

	}

}
