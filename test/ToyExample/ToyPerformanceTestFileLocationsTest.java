package ToyExample;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class ToyPerformanceTestFileLocationsTest {

	@Test
	public void test() {
		assertTrue(new File(ToyPerformanceTestFileLocations.EXEC_CONSTANT).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.EXEC_LINEAR).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.NONSENSE_FILE).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.TOY_INST).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.TOY_RES).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.RES_DIRECTORY).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.EXEC_PYTHON_LINEAR).exists());
		assertTrue(new File(ToyPerformanceTestFileLocations.EXEC_R_CONSTANT).exists());

	}

}
