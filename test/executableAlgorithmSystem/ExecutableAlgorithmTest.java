package executableAlgorithmSystem;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyExampleInputFileTransformer;
import ToyExample.ToyPerformanceTestFileLocations;

public class ExecutableAlgorithmTest {

	ExecutableAlgorithm alg1;
	ExecutableAlgorithm alg2;
	
	String algorithmName1 = "toyAlg1";
	String algorithmName2 = "toyAlg2";
	File exec1;
	File exec2; 
	String exec1Location = ToyPerformanceTestFileLocations.EXEC_CONSTANT;
	String exec2Location = ToyPerformanceTestFileLocations.EXEC_LINEAR;

	String resFileLoc = ToyPerformanceTestFileLocations.RES_DIRECTORY + File.separatorChar + "tempRes" ;
	String errFileLoc = ToyPerformanceTestFileLocations.RES_DIRECTORY + File.separatorChar + "tempErr";

	File resFile, errFile;
	
	String instLoc = ToyPerformanceTestFileLocations.TOY_INST;
	File instFile;
	RunnableInstance instance;
	
	@Before
	public void setUp() throws Exception {
		exec1 = new File(exec1Location);
		exec2 = new File(exec2Location);

		ArrayList<String> prefixes = new ArrayList<String>();
		prefixes.add("java");
		prefixes.add("-jar");
		
		alg1 = new ExecutableAlgorithm.Builder(algorithmName1, exec1)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();
		alg2 = new ExecutableAlgorithm.Builder(algorithmName2, exec2)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();
		
		
		
		resFile = new File(resFileLoc);
		errFile = new File(errFileLoc);
		
		instFile = new File(instLoc);
		instance = null;
		try {
			instance = new RunnableInstance("someInst", instFile);
		} catch (IllegalArgumentException e1) {
			fail();
		} catch (FileNotFoundException e1) {
			fail();
		}

		
				
	}
	
	@Test
	public void testExecutableAlgorithm(){
		//Test if the standard constructor works correctly
		assertTrue(alg1.getName().equals(algorithmName1));
		assertTrue(alg1.getExecutableFile().getAbsoluteFile().equals(exec1.getAbsoluteFile()));
		assertTrue(alg1.getInputFileTransformer()== null);
		assertTrue(alg1.getParameterList().size() == 1); //Only dummy parameter (instance)
		assertTrue(alg1.isInputFilePassedAsParameter());
		assertTrue(alg1.getPositionOfInputFileInParameterList() == 0);
		//Test if error is thrown for invalid file
		boolean thrown = false;
	
		try {
			new ExecutableAlgorithm.Builder(algorithmName1, null).build();
		} catch (IllegalArgumentException e) {
			thrown = true;
		} catch (FileNotFoundException e) {
			fail();
		}
		assertTrue(thrown);
		
		thrown = false;
		try {
			new ExecutableAlgorithm.Builder(algorithmName1, new File("nonsenseFile.blablax")).build();
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
		//Test if error is thrown for invalid parameterlist
		thrown = false;
		try {
			new ExecutableAlgorithm.Builder(algorithmName1, exec1)
			.parameterList(null).build();
		} catch (IllegalArgumentException e) {
			thrown = true;
		} catch (FileNotFoundException e) {
			fail();
		}
		assertTrue(thrown);
		
		//Test correct use
		ArrayList<String> parameterList = new ArrayList<String>();
		parameterList.add("param1");
		ExecutableAlgorithm testAlg = null;
		try {
			testAlg = new ExecutableAlgorithm.Builder(algorithmName1, exec1)
			.parameterList(parameterList).build();
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();;
		}
		assertTrue(testAlg.getParameterList().equals(parameterList));
		//Test deep copy
		parameterList.add("more param");
		assertFalse(testAlg.getParameterList().equals(parameterList));
		
		//Test if error is thrown for invalid use of input file passed as parameter
		//Only set that input file is passed, but no actual location set
		thrown = false;
		try {
			new ExecutableAlgorithm.Builder(algorithmName1, exec1)
			.inputFilePassedAsParameter(true).positionOfInputFileInParameterList(-10).build();
		} catch (IllegalArgumentException e) {
			thrown = true;
		} catch (FileNotFoundException e) {
			fail();;
		}
		assertTrue(thrown);
		
		//Test error thrown when position of input file is >= size of parameter list
		//here: size = 1. Position = 1 => does not exist
		thrown = false;
		ArrayList<String> parameterListTest = new ArrayList<String>();
		parameterListTest.add("instance comes here dummy");
		assertTrue(parameterListTest.size() == 1);
		try {
			new ExecutableAlgorithm.Builder(algorithmName1, exec1)
			.parameterList(parameterListTest).inputFilePassedAsParameter(true).
			positionOfInputFileInParameterList(1).build();
		} catch (IllegalArgumentException e) {
			thrown = true;
		} catch (FileNotFoundException e) {
			fail();;
		}
		assertTrue(thrown);
		
		
		
		//Test correct use
		testAlg = null;
		
		try {
			testAlg = new ExecutableAlgorithm.Builder(algorithmName1, exec1)
			.inputFilePassedAsParameter(true).positionOfInputFileInParameterList(0).build();
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();;
		}
		assertTrue(testAlg.getPositionOfInputFileInParameterList() == 0);
		assertTrue(testAlg.isInputFilePassedAsParameter());
		
		//Test inputFileTransformer		
		ToyExampleInputFileTransformer transformer = new ToyExampleInputFileTransformer();
		testAlg = null;
		
		try {
			testAlg = new ExecutableAlgorithm.Builder(algorithmName1, exec1)
			.inputFileTransformer(transformer).build();
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();;
		}
		assertTrue(testAlg.getInputFileTransformer().equals(transformer));
		
		
	}

	public void testGetParameterList(){
		//Test deep copy
		ArrayList<String> paramList = alg1.getParameterList();
		paramList.add("moreParam");
		assertFalse(paramList.equals(alg1.getParameterList()));
	}

	
	@Test
	public void testBuildProcessFor() {
		

		//alg without parameters
		ProcessBuilder pb = alg1.buildProcessFor(instance, resFile, errFile);
		boolean thrown = false;
		try {
			pb.start(); //Note: this creates external process, so the java executor does not wait for it to finish
		} catch (IOException e) {
			thrown = true;
		}
		assertFalse(thrown); //Assure process can start execution. Does not check for correct execution of the actual code
		
		/*
		//alg with parameters
		Algorithm algWithParam = new Algorithm("someAlgName", exec, paramString);
		ProcessBuilder pb2 = algWithParam.buildProcessFor(instance, runtime);
		thrown = false;
		try {
			pb2.start(); //Note: this creates external process, so the java executor does not wait for it to finish
			//note: the created process will not work because "someParam" is a nonsensical parameter value
			//		But the java code does execute successfully because it is not concerned with the process
		} catch (IOException e) {
			thrown = true;
		}
		assertFalse(thrown); //Assure process can start execution. Does not check for correct execution of the actual code

		//Check that exception is thrown when trying to create an exec for a null-instance
		thrown = false;
		try {
			ProcessBuilder pbTest = alg1.buildProcessFor(null, runtime);
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);
		*/
	}
	
	@Test
	public void testBuildProcessForPythonFile() {
		
		//Python exec alg
		ArrayList<String> prefixes = new ArrayList<String>();
		prefixes.add("python3");
		String pythonExecFileLoc = ToyPerformanceTestFileLocations.EXEC_PYTHON_LINEAR;
		File pythonExec = new File(pythonExecFileLoc);
		ArrayList<String> parameters = new ArrayList<String>();
		parameters.add("instance file dummy");
		ExecutableAlgorithm pythonAlg = null;
		try {
			pythonAlg = new ExecutableAlgorithm.Builder("pythonLinear", pythonExec)
					.parameterList(parameters).inputFilePassedAsParameter(true).positionOfInputFileInParameterList(0)
					 .outputFilePassedAsParameter(false)
					 .prefixesList(prefixes).build();
		} catch (IllegalArgumentException e2) {
			fail();
		} catch (FileNotFoundException e2) {
			fail();
		}
				
		ProcessBuilder pb = pythonAlg.buildProcessFor(instance, resFile, errFile);
		
		boolean thrown = false;
		try {
			pb.start(); //Note: this creates external process, so the java executor does not wait for it to finish
		} catch (IOException e) {
			thrown = true;
		}
		assertFalse(thrown); //Assure process can start execution. Does not check for correct execution of the actual code
		
		/*
		//alg with parameters
		Algorithm algWithParam = new Algorithm("someAlgName", exec, paramString);
		ProcessBuilder pb2 = algWithParam.buildProcessFor(instance, runtime);
		thrown = false;
		try {
			pb2.start(); //Note: this creates external process, so the java executor does not wait for it to finish
			//note: the created process will not work because "someParam" is a nonsensical parameter value
			//		But the java code does execute successfully because it is not concerned with the process
		} catch (IOException e) {
			thrown = true;
		}
		assertFalse(thrown); //Assure process can start execution. Does not check for correct execution of the actual code

		//Check that exception is thrown when trying to create an exec for a null-instance
		thrown = false;
		try {
			ProcessBuilder pbTest = alg1.buildProcessFor(null, runtime);
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);
		*/
	}
	
	@Test
	public void testBuildProcessForRFile() {
		
		//R exec alg
		ArrayList<String> prefixes = new ArrayList<String>();
		prefixes.add("Rscript");
		
		String rExecLocation = ToyPerformanceTestFileLocations.EXEC_R_CONSTANT;
		File RExec = new File(rExecLocation);
		ArrayList<String> parameters = new ArrayList<String>();
		parameters.add("instance file dummy"); //Ignored by const alg in the end
		ExecutableAlgorithm rAlg = null;
		try {
			rAlg = new ExecutableAlgorithm.Builder("RLinear", RExec)
					.parameterList(parameters)
					.inputFilePassedAsParameter(true)
					.positionOfInputFileInParameterList(0)
					.outputFilePassedAsParameter(false)
					.prefixesList(prefixes)
					.build();
		} catch (IllegalArgumentException e2) {
			fail();
		} catch (FileNotFoundException e2) {
			fail();
		}
				
		ProcessBuilder pb = rAlg.buildProcessFor(instance, resFile, errFile);
		
		boolean thrown = false;
		try {
			pb.start(); //Note: this creates external process, so the java executor does not wait for it to finish
		} catch (IOException e) {
			e.printStackTrace();
			thrown = true;
		}
		assertFalse(thrown); //Assure process can start execution. Does not check for correct execution of the actual code
		
		/*
		//alg with parameters
		Algorithm algWithParam = new Algorithm("someAlgName", exec, paramString);
		ProcessBuilder pb2 = algWithParam.buildProcessFor(instance, runtime);
		thrown = false;
		try {
			pb2.start(); //Note: this creates external process, so the java executor does not wait for it to finish
			//note: the created process will not work because "someParam" is a nonsensical parameter value
			//		But the java code does execute successfully because it is not concerned with the process
		} catch (IOException e) {
			thrown = true;
		}
		assertFalse(thrown); //Assure process can start execution. Does not check for correct execution of the actual code

		//Check that exception is thrown when trying to create an exec for a null-instance
		thrown = false;
		try {
			ProcessBuilder pbTest = alg1.buildProcessFor(null, runtime);
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);
		*/
	}
	
}