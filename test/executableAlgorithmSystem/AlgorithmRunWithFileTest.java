package executableAlgorithmSystem;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyPerformanceTestFileLocations;
import performanceDatabase.Algorithm;
import performanceDatabase.Instance;
import performanceDatabase.Performance;
import standardPerformances.MinimisationPerformance;

public class AlgorithmRunWithFileTest {
	Algorithm alg;
	Instance inst;
	Performance perf;
	File resFile;
	String resFileLocation = ToyPerformanceTestFileLocations.TOY_RES;
	AlgorithmRunWithFile run;
	
	@Before
	public void setUp() throws Exception {
		alg = new Algorithm("a1"); 
		inst = new Instance("i1");
		perf = new MinimisationPerformance(10);
		resFile = new File(resFileLocation);		
		run = new AlgorithmRunWithFile(alg, inst, perf, resFile);
	}

	
	@Test
	public void testAlgorithmRunWithFile() {
		  boolean thrown = false;
		  try {
			  new AlgorithmRunWithFile(null, inst, perf, resFile);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  } catch (FileNotFoundException e) {
			fail();
		  }
		  assertTrue(thrown);
		  
		  thrown = false;
		  try {
			  new AlgorithmRunWithFile(alg, null, perf, resFile);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }catch (FileNotFoundException e) {
			fail();
		  }
		  
		  assertTrue(thrown);
	
		  thrown = false;
		  try {
			  new AlgorithmRunWithFile(alg, inst, null, resFile);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }catch (FileNotFoundException e) {
			fail();
		  }
		  assertTrue(thrown);
	
		  thrown = false;
		  try {
			  new AlgorithmRunWithFile(alg, inst, perf, null);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  }catch (FileNotFoundException e) {
			fail();
		  }
		  assertTrue(thrown);
		  
		  //Bogus result file
		  thrown = false;
		  try {
			  new AlgorithmRunWithFile(alg, inst, perf, new File("nonExistantFile.blablax"));
		  } catch (IllegalArgumentException e) {
			  fail();
		  }catch (FileNotFoundException e) {
			thrown = true;
		  }
		  assertTrue(thrown);
	}

	@Test
	public void testGetResultFile() {
		//Apparently comparing a relative path with its equivalent absolute path does not result in an equals() being true
		//Therefore the absolute files are requested
		assertTrue(resFile.getAbsoluteFile().equals(run.getResultFile().getAbsoluteFile()));
	}

}
