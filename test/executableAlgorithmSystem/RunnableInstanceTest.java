package executableAlgorithmSystem;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyPerformanceTestFileLocations;

public class RunnableInstanceTest {

	RunnableInstance i1;
	File file;
	String fileLocation = ToyPerformanceTestFileLocations.TOY_INST;  
	
	
	@Before
	public void setUp() throws Exception {
		file = new File(fileLocation);
		i1 = new RunnableInstance("i1", file);
	}

	@Test
	public void testRunnableInstance() {
		  boolean thrown = false;
		  try {
			  new RunnableInstance("someName", null); //Verifies that illegalargumentexception is indeed thrown
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  } catch (FileNotFoundException e) {
			  fail(); //Null-check should have presedence over checking if the file exists
		  }
		  assertTrue(thrown);
		  
		  thrown = false;
		  try {
			  new RunnableInstance(null, file); //Verifies that illegalargumentexception is indeed thrown
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  } catch (FileNotFoundException e) {
			fail();
		  }
		  assertTrue(thrown);

		  //Checking that a FileNotFoundException is thrown when the passed file does not exist
		  thrown = false;
		  try {
			  new RunnableInstance("tempIName", new File("IAmaNonExistantLocation.blablax")); //Verifies that illegalargumentexception is indeed thrown
		  } catch (IllegalArgumentException e) {
			  fail();
		  } catch (FileNotFoundException e) {
			  thrown = true;
		  }
		  assertTrue(thrown);

		  //Test normal init
		  thrown = false;
		  RunnableInstance testInst = null;
		  String name = "tempIName";
		  try {
			  testInst = new RunnableInstance(name, file);
		  } catch (IllegalArgumentException e) {
			  thrown = true;
		  } catch (FileNotFoundException e) {
			  thrown = true;
		  }
		  assertFalse(thrown);
		  assertTrue(testInst.getInputFile().getAbsoluteFile().equals(file.getAbsoluteFile()));
		  assertTrue(testInst.getName() == name);
		 
	}

	@Test
	public void testGetInputFile() {
		//Apparently comparing a relative path with its equivalent absolute path does not result in an equals() being true
		//Therefore the absolute files are requested
		File newFile = i1.getInputFile().getAbsoluteFile();
		assertTrue(newFile.equals(file.getAbsoluteFile()));
	}

}
