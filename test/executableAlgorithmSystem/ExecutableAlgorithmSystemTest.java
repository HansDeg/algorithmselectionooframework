package executableAlgorithmSystem;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyExamplePerformanceExtractor;
import ToyExample.ToyPerformanceTestFileLocations;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import performanceDatabase.Performance;
import standardPerformances.MinimisationPerformance;

public class ExecutableAlgorithmSystemTest {

	ExecutableAlgorithm alg;
	File exec;
	String execLocation =  ToyPerformanceTestFileLocations.EXEC_LINEAR;
	ToyExamplePerformanceExtractor extractor;

	RunnableInstance inst;
	File instFile;
	String instFileLocation =  ToyPerformanceTestFileLocations.TOY_INST;

	ExecutableAlgorithmSystem sys;
	String probName = "toy problem";
	
	String resDirLoc = ToyPerformanceTestFileLocations.RES_DIRECTORY;
	File resDir;
	
	String runId;
	Performance runPerf;
	AlgorithmRun run;

	
	@Before
	public void setUp() throws Exception {
		exec = new File(execLocation);
		ArrayList<String> prefixes = new ArrayList<String>();
		prefixes.add("java");
		prefixes.add("-jar");
		alg = new ExecutableAlgorithm.Builder("toyAlg", exec)
				.inputFilePassedAsParameter(true)
				.outputFilePassedAsParameter(false)
				.positionOfInputFileInParameterList(0)
				.prefixesList(prefixes)
				.build();

		instFile = new File(instFileLocation);
		inst = new RunnableInstance("i1", instFile);
		
		extractor = new ToyExamplePerformanceExtractor();
		
		resDir = new File(resDirLoc);
		ExecutableAlgorithmSystem.Builder builder = new ExecutableAlgorithmSystem.Builder(probName, resDir);
		sys = builder.build();
		sys.addAlgorithm(alg, extractor);
	
		runPerf = new MinimisationPerformance(10);

		run = new AlgorithmRun(alg, inst, runPerf);

	}

	
	
	
	@Test
	public void testConstructorWithBuilder(){
		//Testing invalid argument exception throwing
		boolean thrown = false;
		try {
			new ExecutableAlgorithmSystem.Builder(null, resDir).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);

		
		thrown = false;
		try {
			new ExecutableAlgorithmSystem.Builder(probName, null).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);

		
		thrown = false;
		try {
			new ExecutableAlgorithmSystem.Builder(probName, new File("notExisting.blablax")).build();
		} catch (FileNotFoundException e) {
			thrown = true;
		} catch (IllegalArgumentException e) {
			fail();
		}
		assertTrue(thrown);

		
		thrown = false;
		try {
			new ExecutableAlgorithmSystem.Builder(probName, resDir).algorithmRunDatabase(null).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
		thrown = false;
		try {
			new ExecutableAlgorithmSystem.Builder(probName, resDir).performanceExtractors(null).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (IllegalArgumentException e) {
			thrown = true;
		}
		assertTrue(thrown);
		
		
		
		//Setting 1: Most basic
		ExecutableAlgorithmSystem sys2 = null;
		try {
			sys2  = new ExecutableAlgorithmSystem.Builder(probName, resDir).build();
		} catch (IllegalArgumentException e) {
			fail();
		}
		catch(FileNotFoundException e){
			fail();
		}
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().size() == 0);
		assertTrue(sys2.getPerformanceExtractors().size() == 0);
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));
		
		//Setting 2: Comes with a list of initial algorithms
		HashMap<ExecutableAlgorithm, PerformanceExtractor> extractors2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		extractors2.put(alg,  extractor);
		sys2 = null;
		try {
			sys2  = new ExecutableAlgorithmSystem.Builder(probName, resDir).performanceExtractors(extractors2).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (IllegalArgumentException e) {
			fail();

		}
	
		assertTrue(sys2.getAlgorithmRunDatabase().getAlgorithmRuns().size() == 0);
		assertTrue(sys2.getPerformanceExtractors().containsKey(alg));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));

		//Setting 3: comes with an initial algorithm run database
		AlgorithmRunDatabase initRuns = new AlgorithmRunDatabase();
		initRuns.addAlgorithmRun(run);
		sys2 = null;
		try {
			sys2  = new ExecutableAlgorithmSystem.Builder(probName, resDir).algorithmRunDatabase(initRuns).build();
		} catch (FileNotFoundException e) {
			fail();
		} catch (IllegalArgumentException e) {
			fail();
		}
		assertTrue(sys2.getAlgorithmRunDatabase().equals(initRuns));
		assertTrue(sys2.getPerformanceExtractors().size() == 0);
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));

		//Normal execution
		initRuns = new AlgorithmRunDatabase();
		ExecutableAlgorithm alg2 = null;
		try {
			alg2 = new ExecutableAlgorithm.Builder("alg2", exec).build();
		} catch (IllegalArgumentException e) {
			fail();

		} catch (FileNotFoundException e) {
			fail();

		}
		AlgorithmRun newRun = new AlgorithmRun(alg2, inst, new MinimisationPerformance(10));
		initRuns.addAlgorithmRun(newRun);
		initRuns.addAlgorithmRun(run);
		extractors2 = new HashMap<ExecutableAlgorithm, PerformanceExtractor>();
		extractors2.put(alg,  extractor);
		extractors2.put(alg2, extractor);
		sys2 = null;
		
		try {
			sys2  = new ExecutableAlgorithmSystem.Builder(probName, resDir).algorithmRunDatabase(initRuns).performanceExtractors(extractors2).build();
		} catch (FileNotFoundException e) {
			fail();

		} catch (IllegalArgumentException e) {
			fail();

		}
		
		assertTrue(sys2.getAlgorithmRunDatabase().equals(initRuns));
		assertTrue(sys2.getPerformanceExtractors().equals(extractors2));
		assertTrue(sys2.getProblemName().equals(probName));
		assertTrue(sys2.getResultFileDirectory().getAbsoluteFile().equals(resDir.getAbsoluteFile()));

		
	}


	
	
	
	@Test
	public void testAddAlgorithm() {
		ExecutableAlgorithm alg2 = null;
		try {
			alg2 = new ExecutableAlgorithm.Builder("a2", exec).build();
		} catch (IllegalArgumentException e) {
			fail();
		} catch (FileNotFoundException e) {
			fail();
		}
		sys.addAlgorithm(alg2, extractor);
		try {
			assertTrue(sys.getPerformanceExtractorFor(alg2).equals(extractor));
		} catch (AlgorithmNotFoundException e) {
			fail();
		}
	}

	@Test
	public void testAddAlgorithmRun() {
		
		
		
		assertFalse(sys.getAlgorithmRunDatabase().getAlgorithmRuns().contains(run));

		sys.addAlgorithmRun(run);
		
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().contains(run));
		
	}

	@Test
	public void testGetAlgorithmRunDatabase() {
		//Check for deep copy
		AlgorithmRunDatabase db = sys.getAlgorithmRunDatabase();
		db.addAlgorithmRun(run);
		assertTrue(db.getAlgorithmRuns().size() == 1);
		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().size() == 0);
	}



	@Test
	public void testExecute() {
		ExecutableAlgorithm alg2 = null;
		try {
			alg2 = new ExecutableAlgorithm.Builder("other alg", exec).build();
		} catch (IllegalArgumentException e1) {
			fail();
		} catch (FileNotFoundException e1) {
			fail();
		}
		
		//Running algorithm not added to system test for exception
		boolean thrown = false;
		AlgorithmRunWithFile testRun = null;
		try {
			testRun = sys.execute(alg2, inst, "testOutputFile", "testErrorFile");
		}catch (InterruptedException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (AlgorithmNotFoundException e) {
			thrown = true;
		} catch (IOException e) {
			fail();
		}
		assertTrue(thrown);
	
		
		try {
			testRun = sys.execute(alg, inst, "testOutputFile", "testErrorFile");
		}catch (InterruptedException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		} catch (AlgorithmNotFoundException e) {
			fail();
		} catch (IOException e) {
			fail();
		}

		assertTrue(sys.getAlgorithmRunDatabase().getAlgorithmRuns().contains(testRun));
		assertTrue(testRun.getAlgorithm().equals(alg));
		assertTrue(testRun.getInstance().equals(inst));

		
	}
	
	/*
	@Test
	public void testExecuteWithJarFile() {
		File jarExec = new File(jarExecLocation);
		Algorithm jarAlg = new Algorithm("jarAlg", jarExec);
		
		sys.addAlgorithm(jarAlg, new DummyExtractor());
		
		AlgorithmRunWithFile resAlgRun = null;

		try {
			resAlgRun = sys.execute(jarAlg, inst);
		} catch (AlgorithmNotInProblemException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		} catch (InvalidFileForPerformanceExtractionException e) {
			fail();
		}
		
		assertTrue(resAlgRun.getAlgorithm().equals(jarAlg));
		assertTrue(resAlgRun.getInstance().equals(inst));
		assertTrue(resAlgRun.getResultFile() != null);
		assertTrue(sys.getAlgorithmRunDatabase().containsRunWithId(resAlgRun.getId()));
		
		List<String> resultfileContent;
		resultfileContent = null;
		try {
			resultfileContent = Files.readAllLines(resAlgRun.getResultFile().toPath());
		} catch (IOException e) {
			fail();
		}
		assertTrue(resultfileContent.get(0).contains("100")); //This is the first line of the output generated by the dummy jar file. If error occurred, somethin else will be printed
		
	}
	*/

	@Test
	public void testGetPerformanceExtractorFor() {
		
		PerformanceExtractor ex = null;
		try {
			ex = sys.getPerformanceExtractorFor(alg);
		} catch (AlgorithmNotFoundException e) {
			fail();
		}
		assertTrue(extractor.equals(ex));
	}
	
	@Test
	public void testGetAvailableAlgorithms(){
		assertTrue(sys.getAvailableAlgorithms().contains(alg));
		assertTrue(sys.getAvailableAlgorithms().size()==1);

	}
	
	
}
