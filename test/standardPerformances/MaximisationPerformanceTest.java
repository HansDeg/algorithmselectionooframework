package standardPerformances;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MaximisationPerformanceTest {

	double objValue1 = 20;
	double objValue2 = 5;
	MaximisationPerformance p1, p2;
	
	@Before
	public void setUp() throws Exception {
		p1 = new MaximisationPerformance(objValue1);
		p2 = new MaximisationPerformance(objValue2);
	}
	
	@Test
	public void testGetValue() {
		assertTrue(p1.getValue() > p2.getValue());
		assertTrue(p1.compareTo(p2) > 0); //p1 is worse than p2 (20 worse than 5 for minimisation)
	}

	@Test
	public void testGetObjectiveValue() {
		assertTrue(objValue1 == p1.getObjectiveValue());
	}

}
