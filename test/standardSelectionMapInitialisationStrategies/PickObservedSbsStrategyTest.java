package standardSelectionMapInitialisationStrategies;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import ToyExample.ToyExamplePerformanceExtractor;
import ToyExample.ToyPerformanceTestFileLocations;
import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import executableAlgorithmSystem.ExecutableAlgorithm;
import executableAlgorithmSystem.RunnableInstance;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import standardPerformances.MinimisationPerformance;
import standardSelectionMaps.SingleAlgorithmSelectionMap;

public class PickObservedSbsStrategyTest {

	ExecutableAlgorithm alg1, alg2;
	File exec1,exec2;
	String execLocation1 = ToyPerformanceTestFileLocations.EXEC_LINEAR;
	String execLocation2 = ToyPerformanceTestFileLocations.EXEC_CONSTANT;
	ToyExamplePerformanceExtractor extractor;

	RunnableInstance inst1, inst2;
	File instFile;
	String instFileLocation = ToyPerformanceTestFileLocations.TOY_INST; 

	AlgorithmRun run1, run2, run3;

	MinimisationPerformance p1,p2,p3;
	
	PickObservedSbsStrategy strategy;
	
	HashSet<ExecutableAlgorithm> availableAlgorithms;
	HashSet<Feature> availableFeatures;
	
	@Before
	public void setUp() throws Exception {
		assertTrue(new File(execLocation1).exists());
		assertTrue(new File(execLocation2).exists());
		assertTrue(new File(instFileLocation).exists());

		exec1 = new File(execLocation1);
		exec2 = new File(execLocation2);
		alg1 = new ExecutableAlgorithm.Builder("alg1", exec1).build();
		alg2 = new ExecutableAlgorithm.Builder("alg2", exec2).build();

		//Dummy instances used: map to instance file is not important as it's never read
		instFile = new File(instFileLocation);
		inst1 = new RunnableInstance("i1", instFile);
		inst2 = new RunnableInstance("i2", instFile);
		
		extractor = new ToyExamplePerformanceExtractor();
		
		p1 = new MinimisationPerformance(100);
		p2 = new MinimisationPerformance(101);
		p3 = new MinimisationPerformance(1000);

		run1 = new AlgorithmRun(alg1, inst1, p1);
		run2 = new AlgorithmRun(alg2, inst1, p2);
		run3 = new AlgorithmRun(alg1, inst2, p3);

		strategy = new PickObservedSbsStrategy();

		availableAlgorithms = new HashSet<ExecutableAlgorithm>();
		availableAlgorithms.add(alg1);
		availableAlgorithms.add(alg2);
		
		availableFeatures = new HashSet<Feature>(); //Empty as features are not used
	}

	
	/*
	 * Tested situation: 
	 * 1 training instances where alg1 slightly better than alg 2
	 * => strategy always selects alg 1
	 * Run alg 1 on new instance, observe bad performance
	 * => average performance of alg 1 is now better
	 * 
	 * Also tested: if insufficient data available to estimate performances: pick random 
	 */
	@Test
	public void testGetSelectionMap() {
		AlgorithmRunDatabase algorithmRunDatabase = new AlgorithmRunDatabase();
		FeatureValueDatabase featureValueDatabase = new FeatureValueDatabase();

		SingleAlgorithmSelectionMap sm = strategy.getSelectionMap(availableAlgorithms, availableFeatures, algorithmRunDatabase, featureValueDatabase);
		ExecutableAlgorithm selectedAlg = sm.getAlgorithm();	

		//Test: if no performance info is available, a random algorihtm is returned
		boolean foundOther = false; //becomes true if a selection mapping is returned that picks the other alg
		for(int i=0; i<10000; i++){
			sm = strategy.getSelectionMap(availableAlgorithms, availableFeatures, algorithmRunDatabase, featureValueDatabase);
			if(!sm.getAlgorithm().equals(selectedAlg)){
				foundOther=true;
				break;
			}
		}
		assertTrue(foundOther);
		
		//test: if performance info for all alg is available, the observed best on average is returned
		algorithmRunDatabase.addAlgorithmRun(run1);
		algorithmRunDatabase.addAlgorithmRun(run2);
		
		sm = strategy.getSelectionMap(availableAlgorithms, availableFeatures, algorithmRunDatabase, featureValueDatabase);
		assertTrue(sm.getAlgorithm().equals(alg1));
		
		//test: if performance info for all alg is available, the observed best on average is returned
		//but now with more data, switching best to alg2

		algorithmRunDatabase.addAlgorithmRun(run3);
		sm = strategy.getSelectionMap(availableAlgorithms, availableFeatures, algorithmRunDatabase, featureValueDatabase);
		assertTrue(sm.getAlgorithm().equals(alg2));
		
		
	}

}
