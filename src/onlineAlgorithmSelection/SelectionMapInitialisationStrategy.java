package onlineAlgorithmSelection;

import java.util.Set;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import algorithmSelectionSystem.SelectionMap;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.AlgorithmRunDatabase;

public interface SelectionMapInitialisationStrategy {

	/**
	 * Returns a selection mapping, predicting algorithms from availableAlgorithms, based on features 
	 * in availableFeatures. 
	 * The algorithmRunDatabase and featureValueDatabase can be used to help initialise the selection mapping
	 * Any implementing subclass must return a selection mapping selecting a subset of availableAlgorithms
	 * and using a subset of availableFeatures to make the selection
	 * 
	 * @param availableAlgorithms: the algorithms from which can be selected. Must contain at least one algorithm.
	 * @param availableFeatures: the features based on which selections can be made.
	 * @param algorithmRunDatabase: a database of algorithm runs that can be helped to initialise the selection mapping
	 * @param featureValueDatabase: a database of feature value records that can be helped to initialise the selection mapping
	 * @return a selection map, selecting a subset of availableAlgorithms based on a subset of abailableFeatures.
	 */
	public abstract SelectionMap getSelectionMap(Set<ExecutableAlgorithm> availableAlgorithms, Set<Feature> availableFeatures,
			AlgorithmRunDatabase algorithmRunDatabase,FeatureValueDatabase featureValueDatabase);

}
