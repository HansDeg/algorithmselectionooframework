package wekaLinking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.AlgorithmRunDatabase;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomTree;

public class WekaSelectionMapBuilders {
	//private static final String WORKING_DIRECTORY = "src\\externalFiles\\resDir\\";
	
	public static WekaClassifierSelectionMap buildClassificationMap(String problemName, Set<ExecutableAlgorithm> availableAlgorithms, Set<Feature> availableFeatures, AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase, String workingDirectory) {
		File targetFile = new File(workingDirectory + problemName + ".arff"); 
		try {
			ProblemToArffFileConvertors.createArffFileForClassification(problemName, availableAlgorithms, availableFeatures, algorithmRunDatabase, featureValueDatabase, targetFile);
			
			//ToDo Mapping of weka alg names to exec alg objects. Needs to be adapted
			//because it now simply creates the relationship based on algorithm names
			//assuming the createArffFileForClassification method generates the weka
			//algorithm names in that way. Same for features
			HashMap<String, ExecutableAlgorithm> wekaNameToExecAlgObjectMap = new HashMap<String, ExecutableAlgorithm>();
			for(ExecutableAlgorithm algorithm: availableAlgorithms){
				wekaNameToExecAlgObjectMap.put(algorithm.getName(), algorithm)	;
			}
			HashMap<String, Feature> wekaNameToFeatureObjectMap = new HashMap<String, Feature>();
			for(Feature feature: availableFeatures){
				wekaNameToFeatureObjectMap.put(feature.getName(), feature);
			}
			
		
			//From http://stackoverflow.com/questions/29380820/concept-behind-arff-and-how-to-read-weka-arff-in-java#29393209
			BufferedReader reader;
			reader = new BufferedReader(new FileReader(targetFile.getAbsolutePath()));
			
			ArffReader arff = new ArffReader(reader);
			Instances data = arff.getData();
			data.setClassIndex(data.numAttributes() - 1); //Last attribute is goal
			
			 String[] options = new String[1];
			 options[0] = "-U";            // unpruned tree
			 J48 tree = new J48();         // new instance of tree
			 try {
				tree.setOptions(options);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}     // set the options
			 try {
				tree.buildClassifier(data);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   // build classifier
			WekaClassifierSelectionMap sm = new WekaClassifierSelectionMap(tree, data,wekaNameToExecAlgObjectMap, wekaNameToFeatureObjectMap  );
			return sm;

		} catch (IOException e1) {
			e1.printStackTrace();
			throw new RuntimeException("Exception thrown that should not happen; could not create arff files. Check code in WekaSelectionMapBuilder: " + e1);
		}
		
	}
	
	
	public static WekaRegressionSelectionMap buildRegressionMap(String problemName, Set<ExecutableAlgorithm> availableAlgorithms, Set<Feature> availableFeatures, AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase, String workingDirectory) {
		HashMap<ExecutableAlgorithm, Classifier> algToRegressionModelMap = new HashMap<ExecutableAlgorithm, Classifier>();
		HashMap<ExecutableAlgorithm,Instances> algToWekaDataMapForRegressions = new HashMap<ExecutableAlgorithm, Instances>();
		
		
		//Create directory where to store all results
		File resultDirectory = new File(workingDirectory +  "weka_models");
		if (!resultDirectory.exists()) {
		    try{
		    	resultDirectory.mkdir();
		    } 
		    catch(SecurityException se){
		    	throw new RuntimeException("Exception thrown that should not happen: " + se);
		    }        
		}
		
		//Creating the mapping of weka feature names to the feature names of the system
		HashMap<String, Feature> wekaNameToFeatureObjectMap = new HashMap<String, Feature>();
		for(Feature feature: availableFeatures){
			wekaNameToFeatureObjectMap.put(feature.getName(), feature);
		}
		
		
		try {
			//For each algorithm a regression model is trained that predicts its performance
			for(ExecutableAlgorithm algorithm: availableAlgorithms){
				String fileName = algorithm.getName() + "_regression.arff";
				File regressionTargetFile = new File(resultDirectory.getAbsolutePath() + "\\" + fileName);
				ProblemToArffFileConvertors.createArffFileForRegression(problemName, algorithmRunDatabase, featureValueDatabase, algorithm, regressionTargetFile);
				
				BufferedReader reader =
						new BufferedReader(new FileReader(regressionTargetFile.getAbsolutePath()));
				ArffReader arff = new ArffReader(reader);
				Instances data = arff.getData();
				data.setClassIndex(data.numAttributes() - 1); //Last attribute is goal
				
				RandomTree regressor = new RandomTree();
				 try {
					 regressor.buildClassifier(data);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("Exception thrown while building weka model: " + e);
				}    
				 
				algToRegressionModelMap.put(algorithm, regressor);
				algToWekaDataMapForRegressions.put(algorithm,  data);
			}
			WekaRegressionSelectionMap sm = new WekaRegressionSelectionMap(algToRegressionModelMap, algToWekaDataMapForRegressions,
					wekaNameToFeatureObjectMap);
			return sm;
		}
		catch (IOException e1) {
			e1.printStackTrace();
			throw new RuntimeException("Exception thrown that should not happen; could not create arff files. Check code in WekaSelectionMapBuilder: " + e1);
		}
		
	}
	
	
	
	
	
	


}
