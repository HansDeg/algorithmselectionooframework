package wekaLinking;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import algorithmSelectionSystem.FeatureValueRecord;
import algorithmSelectionSystem.MissingFeatureForSelectionException;
import algorithmSelectionSystem.SelectionMap;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.Instance;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instances;

public class WekaClassifierSelectionMap implements SelectionMap, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Classifier classifier;
	private Instances wekaData;
	/**
	 * A mapping of the weka names of algorithms at the level of weka
	 * to executable algorithm objects which can be run on the system
	 */
	private HashMap<String, ExecutableAlgorithm> availableAlgorithmsMap;
	/**
	 * A mapping of the weka names of features at the level of weka
	 * to Feature objects which can be used by the system
	 */
	private HashMap<String, Feature> requiredFeaturesMap;
	
	public WekaClassifierSelectionMap(Classifier classifier, Instances wekaData, 
			HashMap<String, ExecutableAlgorithm> availableAlgorithmsMap,
			HashMap<String, Feature> requiredFeaturesMap){
		this.classifier = classifier;
		this.wekaData = wekaData;
		this.availableAlgorithmsMap = availableAlgorithmsMap; //ToDo make deep copy
		this.requiredFeaturesMap = requiredFeaturesMap;
	}
	
	@Override
	public ExecutableAlgorithm selectAlgorithm(Instance instance, HashMap<Feature, Double> featureValues)
			throws MissingFeatureForSelectionException {
		
			FeatureValueDatabase featureValueDatabase = new FeatureValueDatabase();
			for(Feature feature: featureValues.keySet()){
				FeatureValueRecord record = new FeatureValueRecord(instance, feature, featureValues.get(feature));
				featureValueDatabase.addRecord(record);
			}
		
			Instances unlabeled = ProblemToArffFileConvertors.getInstancesObjectForUnlabeledInstance("dummyName", getSelectableAlgorithms(), getRequiredFeatureValues(), featureValueDatabase, instance);
			
			Instances labeled = new Instances(unlabeled);
			 
			// label instances
			double clsLabel = Double.NEGATIVE_INFINITY;
			try {
				clsLabel = classifier.classifyInstance(unlabeled.get(0));
			} catch (Exception e) {
				throw new RuntimeException("Unexpected internal exception in weka. Check code: " + e);
			}
			//Stupid code to obtain the value of the selected algorithm (as all are stored as double)
			labeled.instance(0).setClassValue(clsLabel);
			weka.core.Instance thisWekaInstance = labeled.instance(0);
			String selectedAlgName = thisWekaInstance.stringValue(thisWekaInstance.classAttribute());
			return availableAlgorithmsMap.get(selectedAlgName);
		 
	}

	@Override
	public HashSet<Feature> getRequiredFeatureValues() {
		HashSet<Feature> resSet = new HashSet<Feature>();
		Enumeration<Attribute> algorithmNameEnumerator = wekaData.enumerateAttributes();
		while(algorithmNameEnumerator.hasMoreElements()){
			Attribute nextAttribute = algorithmNameEnumerator.nextElement();
			String attrName = nextAttribute.name();
			Feature nextFeature = requiredFeaturesMap.get(attrName);
			resSet.add(nextFeature);			
		}
		return resSet;
	}

	//Assumes the last attribute is the attribute to select
	@Override
	public HashSet<ExecutableAlgorithm> getSelectableAlgorithms() {
		HashSet<ExecutableAlgorithm> resSet = new HashSet<ExecutableAlgorithm>();
		Enumeration<Object> algorithmNameEnumerator = wekaData.attribute(wekaData.classIndex()).enumerateValues();
		while(algorithmNameEnumerator.hasMoreElements()){
			String nextName = algorithmNameEnumerator.nextElement().toString();
			ExecutableAlgorithm nextAlg = availableAlgorithmsMap.get(nextName);
			resSet.add(nextAlg);			
		}
		return resSet;
		
	}
	
	public String toString(){
		return classifier.toString();
	}

}
