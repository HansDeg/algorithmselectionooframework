package wekaLinking;

import java.io.Serializable;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import algorithmSelectionSystem.FeatureValueRecord;
import algorithmSelectionSystem.MissingFeatureForSelectionException;
import algorithmSelectionSystem.SelectionMap;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.Algorithm;
import performanceDatabase.Instance;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instances;

/**
 * 
 * @author u0075355
 *
 *
 *	@invariant: the keysets of all HashMaps (Algorithm-> Classifier and Algorithm->Weka.Instances) are equal
 *	Defines a selection map based on regression models: for each algorithm a regression model is trained that
 *  predicts its performance. When a selection is made for a new instance, the performance of each algorithm
 *  is predicted and the algorithm with best predicted performance is selected.  
 */
public class WekaRegressionSelectionMap implements SelectionMap, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<ExecutableAlgorithm, Classifier> algToRegressionModelMap;
	private HashMap<ExecutableAlgorithm,Instances> algToWekaDataMapForRegressions;
	private HashMap<String, Feature> requiredFeaturesMap;
	
	
	public WekaRegressionSelectionMap(HashMap<ExecutableAlgorithm, Classifier> algToRegressionModelMap, 
			HashMap<ExecutableAlgorithm,Instances> algToWekaDataMapForRegressions,
			HashMap<String, Feature> requiredFeaturesMap)
	{
		if(algToRegressionModelMap == null) throw new IllegalArgumentException("algToRegressionModelMap cannot be null");
		if(algToWekaDataMapForRegressions == null) throw new IllegalArgumentException("algToWekaDataMapForRegressions cannot be null");
		if(requiredFeaturesMap == null) throw new IllegalArgumentException("requiredFeaturesMap cannot be null");

		if(!algToRegressionModelMap.keySet().equals(algToWekaDataMapForRegressions.keySet())){
			throw new IllegalArgumentException("The mappings from algorithms to weka models or weka data passed to a wekaRegressionSelectionMap  must have exactly the same keyset of algorithms");
		}
		
		this.algToRegressionModelMap = new HashMap<ExecutableAlgorithm, Classifier>(algToRegressionModelMap);
		this.algToWekaDataMapForRegressions = new HashMap<ExecutableAlgorithm, Instances>(algToWekaDataMapForRegressions);
		this.requiredFeaturesMap = requiredFeaturesMap;

	}
	
	@Override
	public HashSet<Feature> getRequiredFeatureValues() {
		//The different classifiers might be based on different attributes
		//So: all classifiers are iterated over and for each of them all features are added
		HashSet<Feature> resSet = new HashSet<Feature>();
		
		//Classifiers
		for(Algorithm algorithm: algToWekaDataMapForRegressions.keySet()){
			//Get attributes used for current algorithm
			Enumeration<Attribute> algorithmNameEnumerator = algToWekaDataMapForRegressions.get(algorithm).enumerateAttributes();
			while(algorithmNameEnumerator.hasMoreElements()){
				Attribute nextAttribute = algorithmNameEnumerator.nextElement();
				String attrName = nextAttribute.name();
				Feature nextFeature = requiredFeaturesMap.get(attrName);
				resSet.add(nextFeature);		
			}
		}
		return resSet;
	}

	@Override
	public Set<ExecutableAlgorithm> getSelectableAlgorithms() {
		return new HashSet<ExecutableAlgorithm>(algToWekaDataMapForRegressions.keySet());
	}
	
	private double getWekaPrediction(Instance instance, HashMap<Feature, Double> featureValues, Classifier classifier, Instances wekaData){

		FeatureValueDatabase featureValueDatabase = new FeatureValueDatabase();
		for(Feature feature: featureValues.keySet()){
			FeatureValueRecord record = new FeatureValueRecord(instance, feature, featureValues.get(feature));
			featureValueDatabase.addRecord(record);
		}
	
		Instances unlabeled = ProblemToArffFileConvertors.getInstancesObjectForUnlabeledInstance("dummyName", getSelectableAlgorithms(), getRequiredFeatureValues(), featureValueDatabase, instance);
		
		// label instances
		double clsLabel = Double.NEGATIVE_INFINITY;
		try {
			clsLabel = classifier.classifyInstance(unlabeled.get(0));
		} catch (Exception e) {
			throw new RuntimeException("Unexpected internal exception in weka. Check code: " + e);
		}
		return clsLabel;
	}

	/**
	 * Selects an algorithm as follows:
	 * 1) Predict for each algorithm if it will solve the instance
	 * 2) 	a) If not a single algorithm is predicted to solve it: select some algorithm
	 * 		b) 	1) Otherwise: predict for all algorithms predicted to solve the instance, what their performance is
	 *			2) Select the predicted best (if multiple are predicted best: select one)
	 *
	 *Note: some order of preference on the algorithms should ideally be defined, in order to
	 *deterministically select an algorithm for all cases. Currently, in case of draws, the first element of a HashSet is selected,
	 *and hashsets don't have a well-defined order	 
	 */
	@Override
	public ExecutableAlgorithm selectAlgorithm(Instance instance, HashMap<Feature, Double> featureValues)
			throws MissingFeatureForSelectionException {
	
		//Get a performance prediction for each algorithm
		HashMap<ExecutableAlgorithm, Double> performancePredictions = new HashMap<ExecutableAlgorithm, Double>();
		for(ExecutableAlgorithm algorithm: algToWekaDataMapForRegressions.keySet()){
			Classifier classifier = algToRegressionModelMap.get(algorithm);
			Instances wekaData = algToWekaDataMapForRegressions.get(algorithm);
			double predictedPerformance = getWekaPrediction(instance, featureValues, classifier, wekaData);
			performancePredictions.put(algorithm, predictedPerformance);
		}
		
		//From http://stackoverflow.com/questions/5911174/finding-key-associated-with-max-value-in-a-java-map
		double maxValueInMap=(Collections.max(performancePredictions.values()));  // This will return max value in the Hashmap
		HashSet<ExecutableAlgorithm> algorithmsWithMaxValue = new HashSet<ExecutableAlgorithm>();
        for (ExecutableAlgorithm algorithm: performancePredictions.keySet()){
        	if(performancePredictions.get(algorithm) == maxValueInMap){
        		algorithmsWithMaxValue.add(algorithm);
        	}
        }
        //Note: if multiple algorithms have the same max value, one of them is returned
       	return algorithmsWithMaxValue.iterator().next();
		
	}
	
	@Override
	public String toString(){
		String res = "Selection mapping based on the following models: \n";
		for(Algorithm algorithm: algToWekaDataMapForRegressions.keySet()){
			res += algorithm + "\n";
			res+= "\t regression model for performance: " + algToRegressionModelMap.get(algorithm).toString() + "\n";
		}
		res += "\n Predicted best algorithm is selected";
		return res;
	}



}
