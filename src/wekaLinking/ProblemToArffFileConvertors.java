package wekaLinking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import algorithmSelectionSystem.FeatureValueRecord;
import executableAlgorithmSystem.ExecutableAlgorithm;
import knowledgeBase.Knowledgebase;
import performanceDatabase.Algorithm;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import performanceDatabase.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;

//Don't forget to change the TEMP_FILE_PATH variable
public class ProblemToArffFileConvertors {
	private static final String COMMENT_FIRST_LINE = "%Arff file automatically generated from a feature value database and a problem, from the performanceDatabase package. ";
	private static final String RELATION_STR = "@relation ";
	private static final String ATTRIBUTE_STR = "@attribute ";
	private static final String ATTRIBUTE_TYPE_NUM = "numeric ";
	public static final String ATTRIBUTE_TO_PREDICT = "bestAlgorithm ";
	private static final String DATA_START_STR = "@data";
	private static final String DATA_SEPARATOR = ",";
	private static final String UNKNOWN_VALUE_STR = "?";
	public static final String REGRESSION_ATTRIBUTE_TO_PREDICT = "performance";
	private static final Path TEMP_FILE_PATH = new File(new File("WekaTempFile").getAbsolutePath()).toPath();
	public static final String FOUND_SOLUTION_TRUE_VALUE = "true";
	public static final String FOUND_SOLUTION_FALSE_VALUE = "false";

	
	
	public static void createArffFileForClassification(String problemName, Set<ExecutableAlgorithm> availableAlgorithms, Set<Feature> availableFeatures, AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase, File targetFile) throws IOException{
		ArrayList<String> fileLines = new ArrayList<String>();		
		fileLines.add(COMMENT_FIRST_LINE);
		addRelationToListOfFileLines(problemName, fileLines);		

		ArrayList<Feature> orderedListOfFeatures = addFeaturesToListOfFileLines(availableFeatures, fileLines);
		/*System.out.println("available Features: " + availableFeatures);
		System.out.println(fileLines);*/
		addAlgorithmAsAttributeToListOfFileLines(availableAlgorithms, fileLines);
		addClassificationDataToListOfFileLines(algorithmRunDatabase, featureValueDatabase, orderedListOfFeatures, fileLines);
		Files.write(Paths.get(targetFile.getAbsolutePath()), fileLines);
		
	}
	

	/*public static void createArffFileForFindsSolutionClassification(String problemName, AlgorithmRunDatabase algorithmRunDatabase,  FeatureValueDatabase featureValueDatabase, Algorithm algorithm, File targetFile) throws IOException{
		ArrayList<String> fileLines = new ArrayList<String>();		
		fileLines.add(COMMENT_FIRST_LINE);
		String relationName = "is_solved_by_" + algorithm.getName();
		addRelationToListOfFileLines(relationName, fileLines);		
		ArrayList<Feature> orderedListOfFeatures = addFeaturesToListOfFileLines(problem.getFeatureSpace(), fileLines);
		HashSet<String> values = new HashSet<String>();
		values.add(FOUND_SOLUTION_TRUE_VALUE);
		values.add(FOUND_SOLUTION_FALSE_VALUE);
		addNominalAttributeToListOfFileLines("solutionFound", values, fileLines);
		addIsSolvedClassificationDataToListOfFileLines(problem, algorithmRunDatabase, featureValueDatabase, orderedListOfFeatures, algorithm, fileLines);
		Files.write(Paths.get(targetFile.getAbsolutePath()), fileLines);
	}*/

	
	public static void createArffFileForRegression(String problemName, AlgorithmRunDatabase algorithmRunDatabase,  FeatureValueDatabase featureValueDatabase, ExecutableAlgorithm targetAlgorithm, File targetFile) throws IOException{
		ArrayList<String> fileLines = new ArrayList<String>();		
		fileLines.add(COMMENT_FIRST_LINE);
		
		addRelationToListOfFileLines(problemName, fileLines);		
		ArrayList<Feature> orderedListOfFeatures = addFeaturesToListOfFileLines(featureValueDatabase.getAllFeatures(), fileLines);
		addRegressionTargetLineToListOfFileLines(fileLines);
		addRegressionDataToListOfFileLines(algorithmRunDatabase, featureValueDatabase, orderedListOfFeatures, targetAlgorithm, fileLines);
		Files.write(Paths.get(targetFile.getAbsolutePath()), fileLines);
	}

	
	/*private static AlgorithmRunDatabase getSolvedRunsRunDatabase(AlgorithmRunDatabase algorithmRunDatabase) {
		HashSet<AlgorithmRun> solvedRuns = new HashSet<AlgorithmRun>();
		for(AlgorithmRun algorithmRun: algorithmRunDatabase.getAlgorithmRuns()){
			if(algorithmRun.getPerformance().foundSolution()){
				solvedRuns.add(algorithmRun);
			}
		}
		return new AlgorithmRunDatabase(solvedRuns);
	}*/
	


	private static void addRegressionTargetLineToListOfFileLines(ArrayList<String> fileLines) {
		String line = ATTRIBUTE_STR + REGRESSION_ATTRIBUTE_TO_PREDICT + " " + ATTRIBUTE_TYPE_NUM;
		fileLines.add(line);		
	}

	
	
	/*public static Instances getInstancesObjectForUnlabeledFoundSolutionInstance(FeatureValueDatabase featureValueDatabase, Instance instance){
		ArrayList<String> fileLines = new ArrayList<String>();		
		String relationName = "is_solved_by_" +problem.getName() + "_unlabeled";
		addRelationToListOfFileLines(relationName, fileLines);		

		ArrayList<Feature> orderedListOfFeatures = addFeaturesToListOfFileLines(featureValueDatabase.getAllFeatures(), fileLines);
		HashSet<String> values = new HashSet<String>();
		values.add(FOUND_SOLUTION_TRUE_VALUE);
		values.add(FOUND_SOLUTION_FALSE_VALUE);
		addNominalAttributeToListOfFileLines("solutionFound", values, fileLines);
		//Creating the data for the to-classify instance
		fileLines.add("");
		fileLines.add(DATA_START_STR);
		fileLines.add("");
		String dataLine = generateDataStringForInstanceAttributes(instance, featureValueDatabase, orderedListOfFeatures);
		dataLine += DATA_SEPARATOR+UNKNOWN_VALUE_STR;
		fileLines.add(dataLine);

		//Writing the file
		try {
			Files.write(TEMP_FILE_PATH, fileLines);
		} catch (IOException e) {
			throw new RuntimeException("An unexpected Input output exception was thrown. Likely cause: static variable TEMP_FILE_PATH was set incorrectly" + e);
		}

		//Reading the file into an instances object
		ArffReader arff = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(TEMP_FILE_PATH.toString()));
			arff = new ArffReader(reader);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("An unexpected FileNotFoundException was thrown. Should not happen because the file that is read is created in the same method" + e);
		} catch (IOException e) {
			throw new RuntimeException("An unexpected Input output exception was thrown. Likely cause: static variable TEMP_FILE_PATH was set incorrectly" + e);
		}
		Instances data = arff.getData();
		data.setClassIndex(data.numAttributes() - 1); //Last attribute is goal

		return data;

	}*/
	/**
	 * 
	 * @param problem
	 * @param featureValueDatabase A feature value database object that must contain one record for each of the features specified in problem's feature space for the instance
	 * @param instance
	 * @return
	 */
	public static Instances getInstancesObjectForUnlabeledInstance(String problemName, Set<ExecutableAlgorithm> availableAlgorithms, HashSet<Feature> availableFeatures, FeatureValueDatabase featureValueDatabase, Instance instance){
		ArrayList<String> fileLines = new ArrayList<String>();		
		addRelationToListOfFileLines(problemName, fileLines);		
		ArrayList<Feature> orderedListOfFeatures = addFeaturesToListOfFileLines(availableFeatures, fileLines);
		addAlgorithmAsAttributeToListOfFileLines(availableAlgorithms, fileLines);
		//Creating the data for the to-classify instance
		fileLines.add("");
		fileLines.add(DATA_START_STR);
		fileLines.add("");
		String dataLine = generateDataStringForInstanceAttributes(instance, featureValueDatabase, orderedListOfFeatures);
		dataLine += DATA_SEPARATOR+UNKNOWN_VALUE_STR;
		fileLines.add(dataLine);

		//Writing the file
		try {
			System.out.println(TEMP_FILE_PATH);
			Files.write(TEMP_FILE_PATH, fileLines);
		} catch (IOException e) {
			throw new RuntimeException("An unexpected Input output exception was thrown. Likely cause: static variable TEMP_FILE_PATH was set incorrectly" + e);
		}

		//Reading the file into an instances object
		ArffReader arff = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(TEMP_FILE_PATH.toString()));
			arff = new ArffReader(reader);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("An unexpected FileNotFoundException was thrown. Should not happen because the file that is read is created in the same method" + e);
		} catch (IOException e) {
			throw new RuntimeException("An unexpected Input output exception was thrown. Likely cause: static variable TEMP_FILE_PATH was set incorrectly" + e);
		}
		Instances data = arff.getData();
		data.setClassIndex(data.numAttributes() - 1); //Last attribute is goal

		return data;
		
		/*
		Instances resInstances;
		
		ArrayList<Attribute> featureAttributes = new ArrayList<Attribute>();
		//Map needed to later add the values to each attribute (to querry the featureValueDatabase) 
		HashMap<Attribute, Feature> mapFromAttributesToCorrespondingFeature = new HashMap<Attribute, Feature>();
		
		//Creating an attribute object for every feature
		for(Feature feature: problem.getFeatureSpace()){
			Attribute newAttribute = new Attribute(feature.getName());
			featureAttributes.add(newAttribute);
			mapFromAttributesToCorrespondingFeature.put(newAttribute, feature);
		}		
		//Creating a nominal attribute object for the target
		ArrayList<String> algorithmNames = new ArrayList<String>();
		for(Algorithm algorithm: problem.getAlgorithmSpace()){
			algorithmNames.add(algorithm.getName());
		}
		Attribute algorithmAttribute = new Attribute(ATTRIBUTE_TO_PREDICT, algorithmNames); //Creates a nominal attribute with possible values specified in 'algorithmNames'
		ArrayList<Attribute> allAttributes = new ArrayList<Attribute>(featureAttributes);
		allAttributes.add(algorithmAttribute);
		
		String instancesName = "unlabeled instance for problem " + problem.getName();
		resInstances = new Instances(instancesName, allAttributes, 1) ;
	    
		
		DenseInstance wekaInstance = new DenseInstance(allAttributes.size());

		// Set instance's values for the attributes "length", "weight", and "position"
		for(Attribute attribute: featureAttributes){
			double attributeValue = featureValueDatabase.getRecord(instance, mapFromAttributesToCorrespondingFeature.get(attribute)).getValue();
			wekaInstance.setValue(attribute, attributeValue);
		}

		// Set instance's dataset to be the dataset "race"
		wekaInstance.setDataset(resInstances); 
		resInstances.add(wekaInstance);
		
		return resInstances;*/
	}
	
	
	

	/**
	 * Adds the line defining the relation
	 * @param relationName
	 * @param fileLines
	 */
	private static void addRelationToListOfFileLines(String relationName , ArrayList<String> fileLines){
		fileLines.add(RELATION_STR + relationName);
		fileLines.add("");
	}
	
	
	/**
	 * Adds a line for each feature in the passed featureSet
	 * Returns an arraylist with the features in the same order as they were added to the file
	 * @param featureSet
	 * @param fileLines
	 * @return
	 */
	private static ArrayList<Feature> addFeaturesToListOfFileLines(Set<Feature> featureSet, List<String> fileLines){
		//Ordered list of features because a hashset has inconsistent ordering
		//and data must be presented in consistent order in the arff format
		ArrayList<Feature> orderedListOfFeatures = new ArrayList<Feature>();
		for(Feature feature:  featureSet){
			String featureName = feature.getName();
			String line = ATTRIBUTE_STR + featureName + " " + ATTRIBUTE_TYPE_NUM;
			fileLines.add(line);
			orderedListOfFeatures.add(feature);
		}
		return orderedListOfFeatures;
	}
	
	
	/**
	 * Adds a nominal attribute, with as values the given names
	 * @param possibleValuesSet
	 * @param fileLines
	 */
	private static void addNominalAttributeToListOfFileLines(String attributeName, HashSet<String> possibleValuesSet, ArrayList<String> fileLines){
		String targetLine = ATTRIBUTE_STR + attributeName + "{";
		boolean firstValue = true;
		for(String algorithmName: possibleValuesSet){
			if(!firstValue){
				targetLine += DATA_SEPARATOR + " " ;
			}
			firstValue = false;
			targetLine += algorithmName;			
		}
		targetLine += "}";
		fileLines.add(targetLine);
	}
	
	
	private static void addClassificationDataToListOfFileLines(AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase, ArrayList<Feature> orderedListOfFeatures, ArrayList<String> fileLines){
 
		fileLines.add("");
		fileLines.add(DATA_START_STR);
		fileLines.add("");
		
		//To get the best algorithm(s) for each instance
		Knowledgebase know = new Knowledgebase(algorithmRunDatabase);
		
		for(Instance instance: algorithmRunDatabase.getAllInstances()){
			HashSet<FeatureValueRecord> recordsOfInstance = featureValueDatabase.getRecordsWithInstance(instance);
			if(!recordsOfInstance.isEmpty()){ //At least one attribute value exists in the database for the instance
				String dataLine = generateDataStringForInstanceAttributes(instance, featureValueDatabase, orderedListOfFeatures);
				
				//Add performance of algorithm
				dataLine = addBestAlgorithmToDataStringForInstanceAttributes(instance, know, dataLine);
				fileLines.add(dataLine);
			}//If empty: do not add the instance
		}
	}
	
	/*private static void addIsSolvedClassificationDataToListOfFileLines(Problem problem, AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase, ArrayList<Feature> orderedListOfFeatures, Algorithm algorithm, ArrayList<String> fileLines){
		 
		fileLines.add("");
		fileLines.add(DATA_START_STR);
		fileLines.add("");
		
		HashSet<AlgorithmRun> allAlgorithmsRuns = algorithmRunDatabase.getAllRunsOfAlgorithm(algorithm);
		HashSet<Instance> instancesWithData = new HashSet<Instance>();
		for(AlgorithmRun run: allAlgorithmsRuns){
			instancesWithData.add(run.getInstance());
		}
		
		for(Instance instance: instancesWithData){
			HashSet<FeatureValueRecord> recordsOfInstance = featureValueDatabase.getRecordsWithInstance(instance);
			if(!recordsOfInstance.isEmpty()){ //At least one attribute value exists in the database for the instance
				String dataLine = generateDataStringForInstanceAttributes(instance, featureValueDatabase, orderedListOfFeatures);
				
				//Check if the algorithm solved the instance, and set attribute accordingly
				AlgorithmRun run = algorithmRunDatabase.getRunsOfAlgOnInst(algorithm, instance).iterator().next();

				if(run.getPerformance().foundSolution()){
					dataLine += DATA_SEPARATOR + FOUND_SOLUTION_TRUE_VALUE;
				}else{
					dataLine += DATA_SEPARATOR + FOUND_SOLUTION_FALSE_VALUE;
				}
				fileLines.add(dataLine);
			}//If empty: do not add the instance
		}
	}*/
	
	public static void addRegressionDataToListOfFileLines(AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase, ArrayList<Feature> orderedListOfFeatures, Algorithm targetAlgorithm, ArrayList<String> fileLines){
		fileLines.add("");
		fileLines.add(DATA_START_STR);
		fileLines.add("");
		
		for(AlgorithmRun algorithmRun: algorithmRunDatabase.getAllRunsOfAlgorithm(targetAlgorithm)){
			Instance instance = algorithmRun.getInstance();
			HashSet<FeatureValueRecord> recordsOfInstance = featureValueDatabase.getRecordsWithInstance(instance);
			if(!recordsOfInstance.isEmpty()){ //At least one attribute value exists in the database for the instance
				String dataLine = generateDataStringForInstanceAttributes(instance, featureValueDatabase, orderedListOfFeatures);
				double performanceValue = algorithmRun.getPerformance().getValue();
				dataLine += DATA_SEPARATOR + performanceValue;
				fileLines.add(dataLine);
			}//If empty: do not add the instance
		}

	}

	/**
	 * Creates and returns a string containing the feature-values for all features specified in orderedListOfFeatures, for the specified instance
	 * static variable 'UNKNOWN_VALUE_STR' is used for features without value for that instance
	 * The values are ordered in the same order as specified in orderedListOfFeatures
	 * @param instance
	 * @param featureValueDatabase
	 * @param orderedListOfFeatures
	 * @return
	 */
	private static String generateDataStringForInstanceAttributes(Instance instance, FeatureValueDatabase featureValueDatabase, ArrayList<Feature> orderedListOfFeatures){
		boolean isFirstElem = true; //No need for data-separator for first element
		String dataLine = "";
		for(Feature feature: orderedListOfFeatures){
			if(!isFirstElem){
				dataLine += DATA_SEPARATOR;
			}
			isFirstElem = false;
			//Add all features' value 1 by one
			FeatureValueRecord record = featureValueDatabase.getRecord(instance, feature);
			if(record == null){
				dataLine += UNKNOWN_VALUE_STR;
			}else{
				dataLine += record.getValue();
			}
		}
		return dataLine;
	}
	
	/**
	 * Adds the attribute 'which algorithm is best for the instance' to a String containing values for other attributes
	 * Specifically: adds the value of static variable 'DATA_SEPARATOR' followed by the name of the best algorithm
	 * If multiple algorithms are best, the first one returned by an iterator over the set of best algorithms is returned
	 * If no algorithm runs are available for that instance, the value of static variable 'UNKNOWN_VALUE_STR' is added
	 * @param instance
	 * @param knowledgebase
	 * @param dataLine
	 * @return
	 */
	private static String addBestAlgorithmToDataStringForInstanceAttributes(Instance instance, Knowledgebase knowledgebase, String dataLine){
		HashSet<Algorithm> bestAlgs = knowledgebase.getBestAlgorithmsOnInstanceBasedOnMeans(instance);
		//Pick the first one if there are multiple
		Algorithm bestAlg = bestAlgs.iterator().next();
		dataLine += DATA_SEPARATOR + bestAlg.getName(); 
		return dataLine;
	}
	
	private static HashSet<String> getSetOfAlgorithmNames(Set<ExecutableAlgorithm> algorithmSet) {
		HashSet<String> names = new HashSet<String>();
		for(ExecutableAlgorithm algorithm: algorithmSet){
			names.add(algorithm.getName());
		}
		return names;
	}
	
	private static void addAlgorithmAsAttributeToListOfFileLines(Set<ExecutableAlgorithm> availableAlgorithms , ArrayList<String> listOfFileLines){
		addNominalAttributeToListOfFileLines(ATTRIBUTE_TO_PREDICT, getSetOfAlgorithmNames(availableAlgorithms), listOfFileLines);
	}
	
	

}
