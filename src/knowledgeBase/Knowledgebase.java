package knowledgeBase;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.NoSuchElementException;

//import algorithmSelectionSystem.FeatureValueDatabase;
import performanceDatabase.Algorithm;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import performanceDatabase.Instance;
import performanceDatabase.Performance;

public class Knowledgebase {
	private AlgorithmRunDatabase algorithmRunDatabase;
	//private FeatureValueDatabase featureValueDatabase;
	
	public Knowledgebase(AlgorithmRunDatabase algorithmRunDatabase){
		this.algorithmRunDatabase = algorithmRunDatabase;
	//	this.featureValueDatabase = null;
	}
	
	/*public Knowledgebase(AlgorithmRunDatabase algorithmRunDatabase, FeatureValueDatabase featureValueDatabase){
		this.algorithmRunDatabase = algorithmRunDatabase;
		this.featureValueDatabase = featureValueDatabase;
	}*/

	
	//Assumes the algorithm has at least 1 performance value on each instance
	public double getMeanPerformanceOfAlgOnInstSet(Algorithm algorithm, HashSet<Instance> instanceSet){
		double perfSum = 0;
		for(Instance inst: instanceSet){
			perfSum += getMeanOfAlgorithmRunRepetitions(algorithm, inst);			
		}		
		return (perfSum/instanceSet.size());		
	}
	
	public Algorithm getSingleBestSolverBasedOnMeans(){
		Algorithm singleBest = null;
		double bestPerf = Double.NEGATIVE_INFINITY;
		
		for(Algorithm algorithm: algorithmRunDatabase.getAllAlgorithms()){
			double perf = getMeanPerformanceOfAlgOnInstSet(algorithm, algorithmRunDatabase.getAllInstances());
			if(singleBest == null || perf>bestPerf){
				bestPerf = perf;
				singleBest = algorithm;
			}
		}
		return singleBest;
	}
	
	public double getMeanVirtualBestPerformanceBasedOnMeans(HashSet<Instance> instanceSet){
		double perfSum = 0;
		for(Instance instance: instanceSet){
			perfSum += getVirtualBestPerformanceOnInstanceBasedOnMeans(instance);
		}
		return perfSum/instanceSet.size();
	}
	
	public double getVirtualBestPerformanceOnInstanceBasedOnMeans(Instance instance){
		Algorithm bestAlg = this.getBestAlgorithmsOnInstanceBasedOnMeans(instance).iterator().next();
		return getMeanOfAlgorithmRunRepetitions(bestAlg, instance);
	}
	
	public HashSet<Instance> getInstancesWithIntraRepetitionDifferences(Algorithm algorithm){
		HashSet<Instance> instancesWithIntraRepDifferences = new HashSet<Instance>();
		for(Instance instance: algorithmRunDatabase.getAllInstances()){
			HashSet<AlgorithmRun> allRuns = algorithmRunDatabase.getRunsOfAlgOnInst(algorithm, instance);
			Performance firstPerfValue = null;
			for(AlgorithmRun run: allRuns){
				Performance perfValue = run.getPerformance();
				if(firstPerfValue == null){
					firstPerfValue = perfValue;
				}
				if(!perfValue.equals(firstPerfValue)){
					instancesWithIntraRepDifferences.add(instance);
				}
			}
		}
		return instancesWithIntraRepDifferences;

	}
	
	public double getMeanOfAlgorithmRunRepetitions(Algorithm algorithm, Instance instance){
		Performance[] allPerformances = getPerformancesInRepetitions(algorithm, instance);
		double sum = 0;
		for(int i = 0; i<allPerformances.length; i++){
			sum += allPerformances[i].getValue();
		}
		return sum/allPerformances.length;
	}
	
	public double[] getDeviationOfAlgorithmRunRepetitions(Algorithm algorithm, Instance instance){
		Performance[] allPerformances = getPerformancesInRepetitions(algorithm, instance);
		double mean = getMeanOfAlgorithmRunRepetitions(algorithm, instance);
		
		double[] result = new double[allPerformances.length];
		for(int i = 0; i<allPerformances.length; i++){
			result[i] = allPerformances[i].getValue() - mean;
		}
		return result;
	}

	public Performance[] getPerformancesInRepetitions(Algorithm algorithm, Instance instance){
		HashSet<AlgorithmRun> allRuns = algorithmRunDatabase.getRunsOfAlgOnInst(algorithm, instance);
		Performance[] performances = new Performance[allRuns.size()];
		int counter = 0;
		for(AlgorithmRun run: allRuns){
			performances[counter] = run.getPerformance();
			counter++;
		}
		return performances; 
		
		
	}
	
		
	public HashSet<Algorithm> getBestAlgorithmsOnInstanceBasedOnMeans(Instance instance){
		if(algorithmRunDatabase.getAllRunsOnInstance(instance).isEmpty()) return null; //No best algorithm
		
		HashSet<Algorithm> bestAlg = new HashSet<Algorithm>();
		double bestPerfValue = Double.NEGATIVE_INFINITY;
		for(Algorithm algorithm: algorithmRunDatabase.getAllAlgorithms()){
			if(algorithmRunDatabase.getRunsOfAlgOnInst(algorithm, instance).size() > 0){ //only consider averages when >1 run present
				double perf = getMeanOfAlgorithmRunRepetitions(algorithm, instance);
				if(perf > bestPerfValue){
					//This algorithm is currently the only best one
					bestPerfValue = perf;
					bestAlg = new HashSet<Algorithm>();
					bestAlg.add(algorithm);
				}
				else if(perf  == bestPerfValue){
					//A second or moreth algorithm is best
					bestAlg.add(algorithm);
				}
				//else: do nothing

			}
		}
		return bestAlg;
	}

	
	public HashSet<Algorithm> getBestAlgorithmsOnInstance(Instance instance){
		if(algorithmRunDatabase.getAllRunsOnInstance(instance).isEmpty()) return null; //No best algorithm
		
		HashSet<Algorithm> bestAlg = new HashSet<Algorithm>();
		double bestPerfValue = Double.NEGATIVE_INFINITY;
		for(AlgorithmRun run: algorithmRunDatabase.getAllRunsOnInstance(instance)){
			if(run.getPerformance().getValue() > bestPerfValue){
				//This algorithm is currently the only best one
				bestPerfValue = run.getPerformance().getValue();
				bestAlg = new HashSet<Algorithm>();
				bestAlg.add(run.getAlgorithm());
			}
			else if(run.getPerformance().getValue() == bestPerfValue){
				//A second or moreth algorithm is best
				bestAlg.add(run.getAlgorithm());
			}
			//else: do nothing
		}
		return bestAlg;
	}
	
	
	public HashSet<Instance> getInstancesOnWhichAlgIsUniqueBest(Algorithm algorithm){
		HashSet<Instance> resInstances = new HashSet<Instance>();
		HashSet<AlgorithmRun> runsToCheck = algorithmRunDatabase.getAllRunsOfAlgorithm(algorithm);
		for(AlgorithmRun run: runsToCheck){
			Instance instance = run.getInstance();
			HashSet<Algorithm> bestAlgsForInst = getBestAlgorithmsOnInstance(instance);
			if(bestAlgsForInst.contains(algorithm) && bestAlgsForInst.size() == 1){
				resInstances.add(instance);
			}
		}
		
		return resInstances;
		
		
	}
	
	/**
	 * Calculates the average performance of the specified algorithm, over all its runs
	 * 	 (=> summed then divided)
	 * @param algorithm
	 * @return the performance averaged over all instances
	 * 	
	 */
	public double getAvgPerformance(Algorithm algorithm){
		double perfSum = 0;
		HashSet<AlgorithmRun> algorithmRuns = algorithmRunDatabase.getAllRunsOfAlgorithm(algorithm);
		for(AlgorithmRun algorithmRun: algorithmRuns){
			perfSum+=algorithmRun.getPerformance().getValue();
		}		
		return perfSum/algorithmRuns.size();
	}
	
	public Algorithm getSingleBestSolver(){
		Algorithm singleBest = null;
		double bestPerf = Double.NEGATIVE_INFINITY;
		
		for(Algorithm algorithm: algorithmRunDatabase.getAllAlgorithms()){
			double perf = getAvgPerformance(algorithm);
			if(singleBest == null || perf>bestPerf){
				bestPerf = perf;
				singleBest = algorithm;
			}
		}
		return singleBest;
	}
	
	public double getVirtualBestAvgPerformance(HashSet<Instance> instanceSet){
		double perfSum = 0;
		for(Instance instance: instanceSet){
			Algorithm bestAlg = this.getBestAlgorithmsOnInstance(instance).iterator().next();
			perfSum += algorithmRunDatabase.getRunsOfAlgOnInst(bestAlg, instance).iterator().next().getPerformance().getValue();
		}
		return perfSum/instanceSet.size();
	}
	
	
	public Performance getVirtualBestPerformance(Instance instance){
		Algorithm bestAlg = this.getBestAlgorithmsOnInstance(instance).iterator().next();
		return algorithmRunDatabase.getRunsOfAlgOnInst(bestAlg, instance).iterator().next().getPerformance();
	}
	
	public void createCsvPerformanceOverviewFile(File targetFile) throws IOException{
		ArrayList<String> fileLines = new ArrayList<String>();
		ArrayList<Algorithm> algorithmList = new ArrayList<Algorithm>(algorithmRunDatabase.getAllAlgorithms()); 
		String firstLine = "  ";
		
		for(Algorithm algorithm: algorithmList){
			firstLine +=",";			
			firstLine += algorithm.getName();			
		}
		Comparator<Instance> comparator = new Comparator<Instance>() {
	        @Override
	        public int compare(Instance inst1, Instance inst2)
	        {
	            return  inst1.getName().compareTo(inst2.getName());
	        }
	    };
	    
		ArrayList<Instance> instanceList = new ArrayList<Instance>(algorithmRunDatabase.getAllInstances());
		Collections.sort(instanceList, comparator);
		fileLines.add(firstLine);
		for(Instance instance: instanceList){
			String content = instance.getName();
			for(Algorithm algorithm: algorithmList){
				content += ",";

				try{
					content +=algorithmRunDatabase.getRunsOfAlgOnInst(algorithm, instance).iterator().next().getPerformance().getValue(); 
				}
				catch(NoSuchElementException e){
					//Don't do anything if there was no performance value there
				}
				}
			fileLines.add(content);
		}
		Files.write(Paths.get(targetFile.getAbsolutePath()), fileLines);
		
	}
}
