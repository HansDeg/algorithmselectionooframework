package performanceDatabase;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/***
 * 
 * @author Hans Degroote
 * Represents a database of algorithmRun objects
 * Defines a number of filter operations on the available algorithm runs
 */
public class AlgorithmRunDatabase  implements Serializable{
	private static final long serialVersionUID = 1L;

	private HashSet<AlgorithmRun> algorithmRuns;
	
	public AlgorithmRunDatabase() {
		this(new HashSet<AlgorithmRun>());
	}
	
	/**
	 * Initialises an algorithmRunDatabase based on the passed collection of algorithmRuns. 
	 * Makes a deep copy of the passed algorithmRuns
	 * 
	 * @param algorithmRuns
	 * @throws IllegalArgumentException if algorithmRuns null
	 */
	public AlgorithmRunDatabase(Collection<AlgorithmRun> algorithmRuns) {
		if(algorithmRuns == null) throw new IllegalArgumentException("AlgorithmRuns cannot be null");
		this.algorithmRuns = new HashSet<AlgorithmRun>(algorithmRuns);
	}


	/**
	 * 
	 * @return a deep copy of the algorithmRuns
	 */
	public HashSet<AlgorithmRun> getAlgorithmRuns(){
		return new HashSet<AlgorithmRun>(algorithmRuns);
	}
	
	/**
	 * Adds the specified run to the algorithm
	 * @param newRun
	 */
	public void addAlgorithmRun(AlgorithmRun newRun) {
		if(newRun == null) throw new IllegalArgumentException("Cannot add a null algorithmRun");
		algorithmRuns.add(newRun);
	}
	
	/**
	 * 	Returns a set containing all algorithms runs of the specified algorithm on the specified instance
	 */
	public HashSet<AlgorithmRun> getRunsOfAlgOnInst(Algorithm alg, Instance inst){
		HashSet<AlgorithmRun> res = new HashSet<AlgorithmRun>();
		for(AlgorithmRun run: algorithmRuns){
			if(inst.equals(run.getInstance()) && run.getAlgorithm().equals(alg)){
				res.add(run);
			}
		}
		return res;
	}
	
	/**
	 * 
	 * @return A set containing all algorithms for which one or more algorithms runs exist
	 */
	public HashSet<Algorithm> getAllAlgorithms(){
		HashSet<Algorithm> res = new HashSet<Algorithm>();
		for(AlgorithmRun run: algorithmRuns){
			res.add(run.getAlgorithm());
		}
		return res;
	}
	
	/**
	 * 
	 * @return A set of all instances for which 1 or more runs exist
	 */
	public HashSet<Instance> getAllInstances(){
		HashSet<Instance> res = new HashSet<Instance>();
		for(AlgorithmRun run: algorithmRuns){
			res.add(run.getInstance());
		}		
		return res;
	}
	
	/**
	 * 
	 * @param algorithm
	 * @return All algorithm runs of the specified algorithm
	 */
	public HashSet<AlgorithmRun> getAllRunsOfAlgorithm(Algorithm algorithm){
		HashSet<AlgorithmRun> res = new HashSet<AlgorithmRun>();
		for(AlgorithmRun run: algorithmRuns){
			if(run.getAlgorithm().equals(algorithm)){
				res.add(run);
			}
		}		
		return res;

	}
	
	/**
	 * 
	 * @param instance
	 * @return all algorithm runs on the specified instance
	 */
	public HashSet<AlgorithmRun> getAllRunsOnInstance(Instance instance){
		HashSet<AlgorithmRun> res = new HashSet<AlgorithmRun>();
		for(AlgorithmRun run: algorithmRuns){
			if(run.getInstance().equals(instance)){
				res.add(run);
			}
		}		
		return res;

	}
	
	/**
	 * 2 algorithm run databases are equal if they contain the exact same algorithm runs
	 */
	@Override 
	public boolean equals(Object other){
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof AlgorithmRunDatabase))
			return false;

		AlgorithmRunDatabase otherDatabase = (AlgorithmRunDatabase) other;
		return this.getAlgorithmRuns().equals(otherDatabase.getAlgorithmRuns());
	}
	
	/**
	 * The hashCode of an algorithm run database is the hashcode of the set of algorithm runs it contains
	 * to make it consistent with the equals method
	 */
	@Override
	public int hashCode(){
		return this.getAlgorithmRuns().hashCode();
	}

	@Override
	public String toString(){
		String res = "Algorithm run database with runs for algorithms :" + this.getAllAlgorithms() + 
				"and instances: " + this.getAllInstances();
		return res;
	}
}
