package performanceDatabase;

import java.io.Serializable;

/**
 * 
 * @author Hans Degroote
 * Defines an abstract notion of 'performance'.
 * A 'performance' is obtained when running an algorithm on an instance, and describes the quality of the run
 * A concrete implementation of Performance must implement the getValue method
 * It is assumed that a bigger value reflects a better performance and an equal value an equal performance
 * It is up to the concrete implementations of Performance to ensure this conversion of better performance to higher value   
 *
 * Note: comparison of performances is done based on double values. Thus even tiny differences in actual
 * value will result in one being smaller or bigger. Overwrite the compareTo method if this is not desired
 */
public abstract class Performance implements Comparable<Performance>, Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * @param other: a non-null performance
	 * @return a value > 0 is this Performance is better than the other
	 * a value == 0 if they are equal
	 * a value < 0 if the this performance is worse than the other
	 */
	public int compareTo(Performance other){
		if(other == null) throw new IllegalArgumentException("The passed performance should be non-null");
		return new Double(getValue()).compareTo(new Double(other.getValue()));
	}
	
	/**
	 * 
	 * @return the value corresponding to the performance
	 */
	public abstract double getValue();
	
	
	/**
	 * Consistent with compareTo: two Performances are equal if the compareTo returns 0
	 */
	public boolean equals(Object other){
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Performance))
			return false;

		Performance otherPerf = (Performance) other;
		return this.compareTo(otherPerf) == 0;
		
	}
	
	@Override
	public int hashCode(){
		return new Double(getValue()).hashCode();
	}
	
	@Override
	public String toString(){
		return "" + getValue();
	}

}
