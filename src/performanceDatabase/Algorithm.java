package performanceDatabase;

import java.io.Serializable;

/**
 * Represents a general algorithm. 
 * An algorithm is defined uniquely by its name.
 * @author Hans Degroote
 *
 */
public class Algorithm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	/**
	 * 
	 * @param name: the name of the new algorithm
	 */
	public Algorithm(String name){
		if(name == null) throw new IllegalArgumentException("An algorithm name cannot be null");
		this.name = name;
	}

	public String getName(){
		return name;
	}
	
	/**
	 * The hashcode of an algorithm is the hashcode of its name
	 */
	@Override
	public int hashCode(){
		return new String(getName()).hashCode();
	}
	
	/**
	 * An algorithm is equal to another algorithm if their names are equal
	 */
	@Override 
	public boolean equals(Object other){
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Algorithm))
			return false;

		Algorithm otherAlg = (Algorithm) other;
		return getName().equals(otherAlg.getName());
	}
	
	@Override
	public String toString(){
		return getName();
	}
}
