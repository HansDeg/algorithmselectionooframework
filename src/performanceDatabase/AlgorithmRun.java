package performanceDatabase;

import java.io.Serializable;

/**
 * 
 * @author Hans Degroote
 * Represents the result of running an algorithm on an instance
 * Specifies for a specific algorithm and instance which performance was obtained
 *
 * A 
 */
public class AlgorithmRun implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Algorithm algorithm;
	private Instance instance;
	private Performance performance;
	
	/**
	 * 
	 * @param algorithm: the algorithm used for the new run
	 * @param instance: the instance used for the new run
	 * @param performance: the performance of the algorithm on the instance in the new run
	 */
	public AlgorithmRun(Algorithm algorithm, Instance instance, Performance performance) {
		
		if(algorithm == null) throw new IllegalArgumentException("Algorithm cannot be null");
		if(instance == null) throw new IllegalArgumentException("Instance cannot be null");
		if(performance == null) throw new IllegalArgumentException("Performance cannot be null");

		this.algorithm = algorithm;
		this.instance = instance;
		this.performance = performance;
	}
	
	public Algorithm getAlgorithm(){
		return algorithm;
	}
	
	public Instance getInstance(){
		return instance;
	}
	
	public Performance getPerformance(){
		return performance;
	}
	
	@Override
	public String toString(){
		String res = algorithm.toString() + " solved " + instance.toString() + " with performance " + performance.toString();
		return res;
	}
	 
}

