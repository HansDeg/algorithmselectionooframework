package performanceDatabase;

import java.io.Serializable;

/**
 * 
 * @author Hans Degroote
 * Represents a general instance
 * An instance is uniquely defined by its name
 * 
 */
public class Instance implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	
	/**
	 * 
	 * @param name: the name of the new instance
	 */
	public Instance(String name) {
		if(name == null){
			throw new IllegalArgumentException("Instance name cannot be null");
		}
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	
	/**
	 * The hashcode of an instance is the hashcode of its name
	 */
	@Override
	public int hashCode(){
		return new String(getName()).hashCode();
	}
	
	/**
	 * Two instances are equal if their names are equal
	 */
	@Override 
	public boolean equals(Object other){
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Instance))
			return false;

		Instance otherInst = (Instance) other;
		return getName().equals(otherInst.getName());
	}
	

	
	@Override
	public String toString(){
		return getName();
	}
}
