package ToyExample;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import algorithmSelectionSystem.FeatureValueExtractor;
import algorithmSelectionSystem.InvalidFileForFeatureValueExtractionException;
import executableAlgorithmSystem.RunnableInstance;

/**
 * 
 * @author Hans Degroote
 * A toy feature value extractor that extracts from a toy instance the problem size
 *
 */
public class ToyFeatureValueExtractor implements FeatureValueExtractor {

	@Override
	public double extractValue(RunnableInstance instance)
			throws InvalidFileForFeatureValueExtractionException, IOException {
		if(instance == null) throw new IllegalArgumentException("The passed instance cannot be null");
		List<String> fileLines = null;
		try {
			fileLines= Files.readAllLines(instance.getInputFile().toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		//Checks if it's a potentially valid file. Less than 2 lines: certainly not		
		if(fileLines.size() < 2) throw new InvalidFileForFeatureValueExtractionException(instance.getInputFile(), this);
		//Extracts the value on the second line (problem size)
		String line2 = fileLines.get(1);
		//Checks that it is indeed problem size
		if(!line2.contains("problem size: ")) throw new InvalidFileForFeatureValueExtractionException(instance.getInputFile(), this);;
		
		int startIndex = line2.indexOf(':') +2;
		int problemSize = 0;
		try{
			problemSize = Integer.parseInt(line2.substring(startIndex, line2.length()));
		}catch(NumberFormatException e){
			throw new InvalidFileForFeatureValueExtractionException("Error when converting the problem size to an integer: " + e, instance.getInputFile(), this);
		}
		return problemSize;
	}

}
