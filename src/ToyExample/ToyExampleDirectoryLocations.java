package ToyExample;

import java.io.File;

public class ToyExampleDirectoryLocations {
	/**
	 * Location of the parent directory of all files related to the toy example
	 * default: directory toyExample in the working directory (standard the project directory)
	 */
	public static String MAIN_DIRECTORY = "toyExampleFiles" + File.separatorChar; 
	public static String INSTANCE_DIRECTORY = MAIN_DIRECTORY + File.separatorChar + "instances" + File.separatorChar;
	public static String TEST_FILES_DIRECTORY = MAIN_DIRECTORY + File.separatorChar + "testFiles" + File.separatorChar;
	public static String EXECUTABLES_DIRECTORY = MAIN_DIRECTORY + File.separatorChar + "executables" + File.separatorChar;
	public static String RESULTS_DIRECTORY = MAIN_DIRECTORY + File.separatorChar + "results" + File.separatorChar;
	
			
}
