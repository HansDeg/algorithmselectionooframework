package ToyExample;

import java.io.File;

import executableAlgorithmSystem.InputFileTransformer;
import executableAlgorithmSystem.RunnableInstance;

/**
 * 
 * @author Hans Degroote
 * Toy input file transformer that doesn't do any transformation, but simply returns the file as is
 *
 */
public class ToyExampleInputFileTransformer implements InputFileTransformer {

	@Override
	public File transformInputFile(RunnableInstance instance) {
		return instance.getInputFile();
	}

}
