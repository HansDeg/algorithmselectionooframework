package ToyExample;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import executableAlgorithmSystem.InvalidFileForPerformanceExtractionException;
import executableAlgorithmSystem.PerformanceExtractor;
import standardPerformances.MinimisationPerformance;

public class ToyExamplePerformanceExtractor extends PerformanceExtractor {

	/**
	 * Extracts the objective value of the result file of the toy problem
	 *  and converts it into a MinimisationPerformance
	 * @param resFile: the file from which the performance should be extracted
	 */
	@Override
	public MinimisationPerformance extractPerformanceImplementation(File resFile)
			throws InvalidFileForPerformanceExtractionException, IOException{
		List<String> allLines = Files.readAllLines(resFile.toPath());
		
		//The third line of the result file contains the objective value
		if(allLines.size() < 2) throw new InvalidFileForPerformanceExtractionException(
				"Result file should be at least 3 lines, with the third line containing the performance",
				resFile, this); 
		String performanceLine = allLines.get(1);
		
		int firstCharOccurrence = performanceLine.indexOf(':')+2;
		double objValue;
		
		
		try{
			objValue = Double.parseDouble(performanceLine.substring(firstCharOccurrence, performanceLine.length()));
		}
		catch(NumberFormatException e){
			throw new InvalidFileForPerformanceExtractionException(resFile, this);
		}
		return new MinimisationPerformance(objValue);
	}

}
