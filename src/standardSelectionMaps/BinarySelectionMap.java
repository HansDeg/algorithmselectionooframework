package standardSelectionMaps;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.MissingFeatureForSelectionException;
import algorithmSelectionSystem.SelectionMap;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.Instance;

/**
 * Defines a selection map that selects between two algorithms based on the value of one feature
 * If the feature value is smaller than a specified crossover value, the first algorithm is selected
 * otherwise, the other algorithm is selected
 * @author u0075355
 *
 */
public class BinarySelectionMap implements SelectionMap, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1;
	private ExecutableAlgorithm algorithm1;
	private ExecutableAlgorithm algorithm2;
	private Feature feature;
	private double crossoverValue; //Value at which point to change from alg1 to alg2 or the other way around
	
	public BinarySelectionMap(ExecutableAlgorithm algorithm1, ExecutableAlgorithm algorithm2, Feature feature, double crossoverValue){
		this.algorithm1 = algorithm1;
		this.algorithm2 = algorithm2;
		this.feature = feature;
		this.crossoverValue = crossoverValue;
	}
	
	public Feature getFeature(){
		return feature;
	}

	/**
	 * Selects algorithm1 if the feature value for the specified feature is smaller than the specified crossover value
	 * Selects algorithm2 otherwise
	 * @throws IllegalArgumentException if featureValues is null
	 * @throws MissingFeatureForSelectionException if featureValues does not contain a value for the required feature
	 * note: instance can be null, since it is never used
	 */
	@Override
	public ExecutableAlgorithm selectAlgorithm(Instance instance, HashMap<Feature, Double> featureValues) throws MissingFeatureForSelectionException {
		if(featureValues == null) throw new IllegalArgumentException("featureValues cannot be null");
		if(!featureValues.containsKey(feature)) throw new MissingFeatureForSelectionException(feature, this);
		
		if(featureValues.get(feature) < crossoverValue){
			return algorithm1;
		}
		else{
			return algorithm2;
		}
	}

	@Override
	public HashSet<Feature> getRequiredFeatureValues() {
		HashSet<Feature> res = new HashSet<Feature>();
		res.add(feature);
		return res; 
	}
	
	public ExecutableAlgorithm getAlgorithm1(){
		return algorithm1;
	}
	
	public ExecutableAlgorithm getAlgorithm2(){
		return algorithm2;
	}
	
	public double getCrossoverValue(){
		return crossoverValue;
	}
	
	@Override
	public HashSet<ExecutableAlgorithm> getSelectableAlgorithms() {
		HashSet<ExecutableAlgorithm> res = new HashSet<ExecutableAlgorithm>();
		res.add(algorithm1);
		res.add(algorithm2);
		return res;
	}

	@Override
	public String toString(){
		String res = "If " + this.getFeature() + " has value <" + this.getCrossoverValue() + ": \n\tpick " + this.getAlgorithm1();
		res += "\nElse: \n\t pick " + this.getAlgorithm2();
		
		return res;
		
	}
}
