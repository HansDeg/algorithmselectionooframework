package standardSelectionMaps;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.SelectionMap;
import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.Instance;

/**
 * 
 * @author Hans Degroote
 * Defines a selection map that always selects the same algorithm
 * Feature values are not considered when making selections 
 *
 */
public class SingleAlgorithmSelectionMap implements SelectionMap, Serializable {
	private static final long serialVersionUID = 1;

	private ExecutableAlgorithm algorithm; //The single algorithm selected for every instance
	
	public SingleAlgorithmSelectionMap(ExecutableAlgorithm algorithm){
		this.algorithm = algorithm;
	}
	
	public ExecutableAlgorithm getAlgorithm(){
		return algorithm;
	}
	
	
	/**
	 * Selects the specified algorithm
	 * Note: instance and featureValues can be null, since they are not used to make the decision
	 */
	@Override
	public ExecutableAlgorithm selectAlgorithm(Instance instance, HashMap<Feature, Double> featureValues) {
		return algorithm;
	}
	
	/**
	 * Returns an empty set of features; for selecting a single algorithm no features are needed
	 */
	@Override
	public HashSet<Feature> getRequiredFeatureValues() {
		return new HashSet<Feature>();
	}

	@Override
	public HashSet<ExecutableAlgorithm> getSelectableAlgorithms() {
		HashSet<ExecutableAlgorithm> res = new HashSet<ExecutableAlgorithm>();
		res.add(algorithm);
		return res;
	}

	@Override
	public String toString(){
		return "Always select " + algorithm;
	}
}
