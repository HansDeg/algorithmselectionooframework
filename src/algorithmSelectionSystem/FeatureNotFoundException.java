package algorithmSelectionSystem;


public class FeatureNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Feature feature;
	
	public FeatureNotFoundException(Feature feature){
		this.feature = feature;
	}
	
	@Override 
	public String toString(){
		return feature + "was not found";
	}
}
