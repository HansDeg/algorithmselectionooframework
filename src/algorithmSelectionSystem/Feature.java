package algorithmSelectionSystem;

import java.io.Serializable;

/**
 * 
 * @author Hans Degroote
 * Represents a feature: a single characteristic of an instance 
 *	A feature is uniquely defined by its name: two features with the same name are the same feature
 */
public class Feature implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String name; 
	
	public Feature(String name){
		if(name == null) throw new IllegalArgumentException("A feature cannot be created with a null name");
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	
	/**
	 * Two features are equal if they have the same name
	 */
	@Override 
	public boolean equals(Object other){
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Feature))
			return false;

		Feature otherFeature = (Feature) other;
		return getName().equals(otherFeature.getName());
	}
	
	@Override
	public int hashCode(){
		return new String(getName()).hashCode();
	}
	
	@Override
	public String toString(){
		return getName();
	}


}
