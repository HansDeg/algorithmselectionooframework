package algorithmSelectionSystem;

import java.util.HashMap;
import java.util.Set;

import executableAlgorithmSystem.ExecutableAlgorithm;
import performanceDatabase.Instance;

/**
 * @author Hans Degroote
 *	Interface for selection mappings
 *	A selection mapping is a mapping of feature values to algorithms
 *	It is defined by implementing the selectAlgorithm method
 * Implementing classes should be immutable
 */
public interface SelectionMap {
	
	/**
	 * @return the algorithm selected for the instance
	 * @throws 	MissingFeatureForSelectionException: if the selection mapping is
	 * 			based on features that are not provided in the given featureValues hashMap
	 */
	public abstract ExecutableAlgorithm selectAlgorithm(Instance instance, HashMap<Feature, Double> featureValues) throws MissingFeatureForSelectionException;

	/**
	 * 
	 * @return a HashSet of features for which the selection map needs a value to make a prediction
	 */
	public abstract Set<Feature> getRequiredFeatureValues(); 
	
	/**
	 * 
	 * @return a set of all executable algorithms that can be selected by the method
	 */
	public abstract Set<ExecutableAlgorithm> getSelectableAlgorithms();
}
