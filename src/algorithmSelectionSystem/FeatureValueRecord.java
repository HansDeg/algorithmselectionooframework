package algorithmSelectionSystem;

import java.io.Serializable;

import performanceDatabase.Instance;

/**
 * @author Hans Degroote
 * Represents a feature value record
 * A feature value record consists of a value for a specific feature
 * 	on a specific instance
 *
 */
public class FeatureValueRecord implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Feature feature;
	private Instance instance;
	private double value;

	public FeatureValueRecord(Instance instance, Feature feature, double value){
		if(feature == null) throw new IllegalArgumentException("No FeatureValueRecord can be created with a null feature");
		if(instance == null) throw new IllegalArgumentException("No FeatureValueRecord can be created with a null instance");

		this.feature = feature;
		this.instance = instance;
		this.value = value;
	}
	
	public Feature getFeature(){
		return feature;
	}
	
	public Instance getInstance(){
		return instance;
	}
	
	public double getValue(){
		return value;
	}
	

	@Override
	public String toString(){
		String res = "The value of feature " + feature.toString() + " on instance " + instance.toString() + " is: " + value;   
		return res;
	}
	
	
}
