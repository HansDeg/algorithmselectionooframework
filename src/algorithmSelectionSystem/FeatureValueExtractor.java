package algorithmSelectionSystem;


import java.io.IOException;

import executableAlgorithmSystem.RunnableInstance;

/**
 * @author Hans Degroote
 * Represents an interface for feature value extractors
 * A feature value extractors extracts for a given runnable instance
 * what the value for a particular feature is

 */
public interface FeatureValueExtractor {
	
	/**
	 * 
	 * @param inst: the instance from which the feature value should be extracted
	 * @return: the value of the feature for the given instance
	 * @throws 	InvalidFileForFeatureValueExtractionException: if the file corresponding to the instance exists, but no
	 * 			performance can be extracted from it
	 * @throws 	IOException: an error occurred during the reading of the input file
	 * @Pre:	The file contained in the runnableInstance exists and if a file 
	 */
	public abstract double extractValue(RunnableInstance instance) throws InvalidFileForFeatureValueExtractionException, IOException;

}
