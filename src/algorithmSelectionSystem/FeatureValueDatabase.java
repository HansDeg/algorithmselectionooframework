package algorithmSelectionSystem;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import performanceDatabase.Instance;

/**
 * 
 * @author Hans Degroote
 * Represents a database of feature value records
 */
public class FeatureValueDatabase implements Serializable{
	private static final long serialVersionUID = 1L;
	private HashSet<FeatureValueRecord> featureValueRecords;
	
	public FeatureValueDatabase(Set<FeatureValueRecord> initRecords){
		this.featureValueRecords = new HashSet<FeatureValueRecord>(initRecords);
	}
	
	public FeatureValueDatabase(){
		this(new HashSet<FeatureValueRecord>());
	}
	
	/**
	 * 
	 * @return a deep copy of the feature value records 
	 */
	public HashSet<FeatureValueRecord> getFeatureValueRecords(){
		return new HashSet<FeatureValueRecord>(featureValueRecords);
	}
	
	public void addRecord(FeatureValueRecord newRecord){
		if(newRecord == null) throw new IllegalArgumentException("Cannot add a null-record to a featureValueDatabase");
		featureValueRecords.add(newRecord);
	}
	
	/**
	 * 
	 * @return a HashSet of all features for which at least 1 record exists
	 */
	public HashSet<Feature> getAllFeatures(){
		HashSet<Feature> allFeatures = new HashSet<Feature>();
		for(FeatureValueRecord record: featureValueRecords){
			allFeatures.add(record.getFeature());
		}
		return allFeatures;
	}
	
	/**
	 * 
	 * @return a HashSet of all instances for which at least 1 record exists
	 */
	public HashSet<Instance> getAllInstances(){
		HashSet<Instance> allInst = new HashSet<Instance>();
		for(FeatureValueRecord record: featureValueRecords){
			allInst.add(record.getInstance());
		}
		return allInst;

	}

	/**
	 * Returns a hashset of all feature records for the specified instance
	 * @param instance
	 * @return
	 */
	public HashSet<FeatureValueRecord> getRecordsWithInstance(Instance instance) {
		HashSet<FeatureValueRecord> res = new HashSet<FeatureValueRecord>();
		for(FeatureValueRecord record: featureValueRecords){
			if(record.getInstance().equals(instance)){
				res.add(record);
			}
		}
		return res;
	}
	
	/**
	 * Returns the featureValueRecord of the specified feature and instance
	 * If they match multiple feature record, the first one is returned
	 * If they match no feature record, null is returned
	 */
	public FeatureValueRecord getRecord(Instance instance, Feature feature){
		for(FeatureValueRecord record: featureValueRecords){
			if(record.getInstance().equals(instance) && record.getFeature().equals(feature)){
				return record;
			}
		}
		return null;

	}
	
	
	/**
	 * 2 Feature value databases are equal if they contain the exact same algorithm runs
	 */
	@Override 
	public boolean equals(Object other){
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof FeatureValueDatabase))
			return false;

		FeatureValueDatabase otherDatabase = (FeatureValueDatabase) other;
		return this.getFeatureValueRecords().equals(otherDatabase.getFeatureValueRecords());
	}
	
	/**
	 * The hashCode of an algorithm run database is the hashcode of the set of algorithm runs it contains
	 * to make it consistent with the equals method
	 */
	@Override
	public int hashCode(){
		return this.getFeatureValueRecords().hashCode();
	}
	
	@Override
	public String toString(){
		String res = "Feature value database with values for features: " + getAllFeatures();
		res += " and for instances: " + getAllInstances();
		return res;
	}
}
