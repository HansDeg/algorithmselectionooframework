package algorithmSelectionSystem;

import java.io.File;


public class InvalidFileForFeatureValueExtractionException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8791873095781760560L;

	private File file;
	private FeatureValueExtractor extractor;
	
	public InvalidFileForFeatureValueExtractionException(File file, FeatureValueExtractor extractor){
		this("", file, extractor);
	}
	
	public InvalidFileForFeatureValueExtractionException(String message, File file, FeatureValueExtractor extractor){
		super(message + "\nNo performance could be extracted from file " + file + " using extractor " + extractor);
		this.file = new File(file.getAbsolutePath());
		this.extractor = extractor;
	}

	
	public File getFile(){
		return new File(file.getAbsolutePath());
	}
	
	public FeatureValueExtractor getPerformanceExtractor(){
		return extractor;
	}
	

}
