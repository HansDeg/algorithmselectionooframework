package algorithmSelectionSystem;

import java.util.HashSet;
import java.util.Set;

import executableAlgorithmSystem.ExecutableAlgorithm;

/**
 * 
 * @author Hans Degroote
 * 
 * Represents an exception thrown when a selection map is not valid for an algorithm selection system
 *
 */
public class InvalidSelectionMapException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private HashSet<ExecutableAlgorithm> algorithmsAvailableInSys;
	private HashSet<Feature> featuresAvailableInSys;
	private SelectionMap selectionMap;
	
	public InvalidSelectionMapException(SelectionMap selectionMap, Set<Feature> featuresAvailableInSys,
			Set<ExecutableAlgorithm> algorithmsAvailableInSys){
		this.selectionMap = selectionMap;
		this.featuresAvailableInSys = new HashSet<Feature>(featuresAvailableInSys);
		this.algorithmsAvailableInSys = new HashSet<ExecutableAlgorithm>(algorithmsAvailableInSys);		
	}
	
	@Override
	public String toString(){
		String res = "Selection map " + selectionMap + " cannot be used for the algorithm selection system. ";
		res += "\n The system can extract features: " + featuresAvailableInSys;
		res += "\n The system can run algorithms: " + algorithmsAvailableInSys;
		return res;
	}

}
