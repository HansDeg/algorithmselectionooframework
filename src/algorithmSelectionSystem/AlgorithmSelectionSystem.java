package algorithmSelectionSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import executableAlgorithmSystem.AlgorithmNotFoundException;
import executableAlgorithmSystem.AlgorithmRunWithFile;
import executableAlgorithmSystem.ExecutableAlgorithm;
import executableAlgorithmSystem.ExecutableAlgorithmSystem;
import executableAlgorithmSystem.InvalidFileForPerformanceExtractionException;
import executableAlgorithmSystem.PerformanceExtractor;
import executableAlgorithmSystem.RunnableInstance;
import performanceDatabase.AlgorithmRunDatabase;

/**
 * 
 * @author Hans Degroote
 * 
 * Represents an algorithm selection system: a system that can automatically select the (predicted) best 
 * 	algorithm to solve an instance with
 * 
 * @invariant The selection map selects only algorithms for which a performance extractor is available
 * @invariant The selection map requires only features for which a feature value extractor is available
 * Or in other words: 
 * @invariant isValidSelectionMapping(this.selectionMapping) == true
 *
 */
public class AlgorithmSelectionSystem extends ExecutableAlgorithmSystem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SelectionMap selectionMap;
	private HashMap<Feature, FeatureValueExtractor> featureValueExtractorMap;
	private FeatureValueDatabase featureValueDatabase;

	/**
	 * Initialisation through builder pattern
	 * Initialises an algorithm selection system based on the data contained in the builder
	 * @param builder: the builder based on which the system should be initialised
	 * @throws FileNotFoundException	if the directory in which results should be stored does not exist
	 * @throws InvalidSelectionMapException	if the proposed selection map is incompatible with the system
	 */
	protected AlgorithmSelectionSystem(Builder builder) throws FileNotFoundException, InvalidSelectionMapException{
		super(builder.superBuilder);
		if(builder.selectionMap == null) throw new IllegalArgumentException("Selection map cannot be null");
		if(builder.featureValueExtractorMap == null) throw new IllegalArgumentException("featureValueExtractorMap cannot be null");
		if(builder.featureValueDatabase == null) throw new IllegalArgumentException("Feature value database cannot be null");

		this.featureValueExtractorMap = builder.featureValueExtractorMap;
		this.featureValueDatabase = builder.featureValueDatabase;
		setSelectionMap(builder.selectionMap);
	}
		
	public SelectionMap getSelectionMap(){
		return selectionMap;
	}
	
	/**
	 * Returns a deep copy of the feature value extractor map
	 * @return
	 */
	public HashMap<Feature, FeatureValueExtractor> getFeatureValueExtractorMap(){
		HashMap<Feature, FeatureValueExtractor> deepCopy = new HashMap<Feature, FeatureValueExtractor>();
		for(Feature feature: featureValueExtractorMap.keySet()){
			deepCopy.put(feature, featureValueExtractorMap.get(feature));
		}
		return deepCopy;
	}
	
	/**
	 * A selection map is valid if all features it requires can be extracted by the system AND
	 * 		if all algorithms it can predict can be executed from the system  
	 * Concretely: if for each of the required features, a feature value extractor is available AND
	 * 		if for each of the possibly predicted algorithms, an ExecutableAlgorithm is available
	 * Note: a null selectionMap is always invalid
	 * @param selectionMap: the selection map for which must be tested if it's valid for the system
	 * @return: true if the selection map is valid for the system, false otherwise
	 */
	public boolean isValidSelectionMap(SelectionMap selectionMap){
		if(selectionMap == null) return false;
		
		boolean hasOkFeatures = this.featureValueExtractorMap.keySet().containsAll(selectionMap.getRequiredFeatureValues());
		boolean hasOkAlgorithms = super.getPerformanceExtractors().keySet().containsAll(selectionMap.getSelectableAlgorithms());
		
		return hasOkFeatures && hasOkAlgorithms;
	}
	
	/**
	 * Sets the selection map of the system to be the specified selection map if it is a valid one
	 * Throws an exception (and does not change the system) if the selectionMap is not valid for the system 
	 * @param selectionMap: the selection map that should be added to the system
	 * @throws InvalidSelectionMapException if the selection map is not valid
	 * 			concretely: if iValidSelectionMap(selectionMap) == false
	 */
	public void setSelectionMap(SelectionMap selectionMap) throws InvalidSelectionMapException{
		if(!isValidSelectionMap(selectionMap)){
			throw new InvalidSelectionMapException(selectionMap, this.featureValueExtractorMap.keySet(), 
					super.getPerformanceExtractors().keySet());
		}
		
		this.selectionMap = selectionMap;
	}
	
	/**
	 * Adds a new feature value record to the system. 
	 * Adds the instance for which the value is calculated to the system if it had not yet been added
	 * @param newRecord: the record that should be added to the system
	 * @throws FeatureNotInProblemException: if the feature for which the record should be added is not a part of the system
	 */
	public void addFeatureValueRecord(FeatureValueRecord newRecord){
		featureValueDatabase.addRecord(newRecord);
	}
	
	/**
	 * 
	 * @return a deep copy of the featureValueDatabase
	 */
	public FeatureValueDatabase getFeatureValueDatabase(){
		return new FeatureValueDatabase(new HashSet<FeatureValueRecord>(featureValueDatabase.getFeatureValueRecords()));
	}

	/**
	 * Returns the feature extractor corresponding to the specified feature
	 * @param feature: the feature for which the feature extractor is requested
	 * @return: the feature extractor corresponding to the specified feature
	 * @throws FeatureNotFoundException: no feature extractor exists for the specified feature
	 */
	public FeatureValueExtractor getFeatureValueExtractorFor(Feature feature) throws FeatureNotFoundException{
		if(!featureValueExtractorMap.containsKey(feature)) throw new FeatureNotFoundException(feature); 
		return featureValueExtractorMap.get(feature);
	}
	
	/**
	 *  Adds a new feature to the system, adding it to the problem and linking it with the given FeatureValueExtractor
	 *  Note: if the feature already exists, its corresponding extractor will be changed to the passed extractor
	 * @param feature
	 * @param extractor
	*/
	public void addFeature(Feature feature, FeatureValueExtractor extractor){
		if(feature == null) throw new IllegalArgumentException("A null feature cannot be added to an algorithm selection system");
		if(extractor == null) throw new IllegalArgumentException("A feature cannot have a null value extractor");
		featureValueExtractorMap.put(feature, extractor);
	}

	
	/**
	 * Processes a new instance, by first selection an algorithm for it and then executing that algorithm,
	 * 		by calling the execute method of superclass ExecutableAlgorithmSelectionSystem
	 * Stores the resulting algorithm run in the algorithm run database
	 * Stores the feature values calculated to make the selection in the internal feature value record database
	 * @param instance	the instance that should be processed
	 * @param logFile	The file where the result of the run should be written
	 * @return	the algorithm run for the selected algorithm on the instance
	 * @result	The algorithm run for the selected algorithm has been added to the algorithmRunDatabase
	 * @result	For each feature required by the selection mapping, a featureValueRecord has been added to the 
	 * 			featureValueRecordDatabase
	 * @throws InterruptedException: the execution of the algorithm on the instance was forcible interrupted
	 * @throws InvalidFileForPerformanceExtractionException: the selected algorithm generated a result file from
	 * 			which no performance could be extracted
	 * @throws InvalidFileForFeatureValueExtractionException: one of the required features' value could not
	 * 			be extracted from the instance's file.
	 * @throws IOException: the process to execute the algorithm could not be created by the ProcessBuilder
	 * @throws InvalidselectionMapException: the current selection mapping is invalid
	 */
	public AlgorithmRunWithFile processInstance(RunnableInstance instance, String outputFileName, String errLogFileName) throws 
	InterruptedException, InvalidFileForPerformanceExtractionException, InvalidFileForFeatureValueExtractionException, 
	IOException, InvalidSelectionMapException{
        if(instance == null) throw new IllegalArgumentException("The passed instance cannot be null");
        if(outputFileName == null) throw new IllegalArgumentException("The passed outputFileName cannot be null");
        if(errLogFileName == null) throw new IllegalArgumentException("The passed errLogFileName cannot be null");

        
        //Checking if the selection mapping is valid
        //This is checked when it is first added to the system, but it is theoretically possible for the system
        //to change in such a way that it no longer is valid, for example if an algorithm is removed        
        if(!isValidSelectionMap(selectionMap)){
			throw new InvalidSelectionMapException(selectionMap, this.featureValueExtractorMap.keySet(), 
					super.getPerformanceExtractors().keySet());
        }

		Set<Feature> requiredFeatures = selectionMap.getRequiredFeatureValues();
					
		//extracting all required features
		HashMap<Feature, Double> featureValueMap;
		try {
			featureValueMap = getFeatureValueMapForInstance(instance, requiredFeatures);
		} catch (FeatureNotFoundException e1) {
			//Should not happen, because the selection mapping should only request features that are 
			//available in the system. See also: isValidSelectionMapping method
			throw new RuntimeException("Error thrown that should not happen: " + e1 + "\n. "
					+ "Thrown because a feature required by the selection mapping cannot be extracted in the system. "
					+ "This means the algorithm selection system is in an inconsistent state it should never enter. "
					+ "Please contact the author of the code.");
		}
		
		ExecutableAlgorithm algToUse = null;
		try {
			algToUse = selectionMap.selectAlgorithm(instance, featureValueMap);
		} catch (MissingFeatureForSelectionException e) {
			//Should not happen, because only selection mappings that are valid can be used by the system
			//See also: isValidSelectionMapping method
			throw new RuntimeException("Error thrown that should not happen: " + e + "\n. "
					+ "Thrown because a feature required by the selection mapping cannot be extracted in the system. "
					+ "This means the algorithm selection system is in an inconsistent state it should never enter. "
					+ "Please contact the author of the code.");
		}
		try {
			return super.execute(algToUse, instance, outputFileName, errLogFileName);
		} catch (AlgorithmNotFoundException e) {
			//Should not happen, because only selection mappings that are valid can be used by the system
			//See also: isValidSelectionMapping method
			throw new RuntimeException("Error thrown that should not happen: " + e + "\n."
					+ " Thrown because an algorithm predicted by the the selection mapping was not available."
					+ " This means the algorithm selection system is in an inconsistent state it should never enter. "
					+ "Please contact the author of the code.");
		}
		
	}
	
	
	/**
	 * Returns a map containing values for all features specified in requiredFeatures
	 * Also adds all feature values to the database
	 * @param instance: the instance for which the feature values should be calculated
	 * @param requiredFeatures: the features that should be calculated for the instance
	 * @return: a map containing for each feature in requiredFeatures the value for instance
	 * @throws InvalidFileForFeatureValueExtractionException: no feature value could be extracted from the instance file for one of the features
	 * @throws IOException
	 * @throws FeatureNotFoundException: one of the requested features was not contained in the system (did not have a corresponding FeatureValueExtractor) 
	 */
	private HashMap<Feature,Double> getFeatureValueMapForInstance(RunnableInstance instance, Set<Feature> requiredFeatures)
			throws InvalidFileForFeatureValueExtractionException, IOException, FeatureNotFoundException{
		HashMap<Feature, Double> featureValueMap = new HashMap<Feature, Double>();
		for(Feature feature: requiredFeatures){
			double featureValue;
			featureValue = getFeatureValueExtractorFor(feature).extractValue(instance);
			
			featureValueMap.put(feature, featureValue);
			FeatureValueRecord newRec = new FeatureValueRecord(instance, feature, featureValue);
						addFeatureValueRecord(newRec);
			
		}
		return featureValueMap;
	}
	
	/**
	 * 
	 * @return the set of features for which the value can be extracted by this system 
	 */
	public Set<Feature> getAvailableFeatures(){
		return featureValueExtractorMap.keySet();
	}
	
	
	@Override
	public String toString(){
		String res = "Algorithm selection system for problem " + getProblemName();
		res += "\nselection mapping: ";
		res += getSelectionMap();
		res += "\nresult file directory: " + getResultFileDirectory() +"\n";
		res += getAlgorithmRunDatabase().toString() + "\n";
		res += getFeatureValueDatabase();
		return res;
	}
	
	
	/*
	 * Note: this builder shares much functionality with the builder of ExecutableAlgorithmSystem
	 * Code is duplicated here instead of using a more elegant solution such as inheritance to avoid complication
	 * This would be the 'good' solution: https://chrononaut.org/2013/03/19/subclassing-with-blochs-builder-pattern-revised/
	 * This would be the 'normal' solution: http://stackoverflow.com/questions/17164375/subclassing-a-java-builder-class#
	 */
	public static class Builder{
		
		//Super class builder
		private ExecutableAlgorithmSystem.Builder superBuilder;
		 
		//Parameters from this class
		private SelectionMap selectionMap;
		private HashMap<Feature, FeatureValueExtractor> featureValueExtractorMap;
		private FeatureValueDatabase featureValueDatabase;
		
		/**
		 * Initialises the builder based on the given problemName and resultFileDirectory
		 * @param problemName: the problemname of the algorithm selection system
		 * @param resultFileDirectory: the directory where the result files generated by the system
		 * 			should be stored
		 */
		public Builder(String problemName, File resultFileDirectory){
			superBuilder = new ExecutableAlgorithmSystem.Builder(problemName, resultFileDirectory);
			selectionMap = null;
			featureValueExtractorMap = new HashMap<Feature, FeatureValueExtractor>();
			featureValueDatabase = new FeatureValueDatabase();
		}

		/**
		 * Initialises the content of the eventual algorithm selection system based on
		 * an executable algorithm system
		 * 
		 */
		public Builder(ExecutableAlgorithmSystem executableAlgorithmSys){
			superBuilder = new ExecutableAlgorithmSystem.Builder(executableAlgorithmSys.getProblemName(),
					executableAlgorithmSys.getResultFileDirectory());
			superBuilder = superBuilder.performanceExtractors(executableAlgorithmSys.getPerformanceExtractors());
			superBuilder = superBuilder.algorithmRunDatabase(executableAlgorithmSys.getAlgorithmRunDatabase());
			
			//Own parameters
			selectionMap = null;
			featureValueExtractorMap = new HashMap<Feature, FeatureValueExtractor>();
			featureValueDatabase = new FeatureValueDatabase();
		}

		
		
		/**
		 * Adds a deep copy of the provided map to the Builder object 
		 * @param performanceExtractors
		 * @return The updated Builder object, incorporating a deep copy of the passed map
		 */
		public Builder performanceExtractors(HashMap<ExecutableAlgorithm, PerformanceExtractor> performanceExtractors){
			superBuilder = superBuilder.performanceExtractors(performanceExtractors);
			/*this.performanceExtractors = new HashMap<Algorithm, PerformanceExtractor>();
			for(Algorithm alg: performanceExtractors.keySet()){
				this.performanceExtractors.put(alg, performanceExtractors.get(alg));
			}*/
			return this;
		}
		
		/**
		 * 
		 * @return	a deep copy of the performance extractors
		 */
		public HashMap<ExecutableAlgorithm, PerformanceExtractor> getPerformanceExtractors(){
			return superBuilder.getPerformanceExtractors();
		}
		
		/**
		 * Adds a deep copy of the provided algorithmRunDatabase to the Builder
		 * @param algorithmRunDatabase
		 * @return The updated Builder object, incorporating a deep copy of the passed map
		 */
		public Builder algorithmRunDatabase(AlgorithmRunDatabase algorithmRunDatabase){
			superBuilder = superBuilder.algorithmRunDatabase(algorithmRunDatabase);
			//this.algorithmRunDatabase = new AlgorithmRunDatabase(algorithmRunDatabase.getAlgorithmRuns());
			return this;
		}
		
		/**
		 * 
		 * @return	a deep copy of the database of algorithm runs
		 */
		public AlgorithmRunDatabase getAlgorithmRunDatabase(){
			return superBuilder.getAlgorithmRunDatabase();
		}
		
		/**
		 * Adds the provided selectionMap to the Builder
		 * @param selectionMap
		 * @return The updated Builder object, incorporating the selection map
		 */
		public Builder selectionMap(SelectionMap selectionMap){
			this.selectionMap = selectionMap;
			return this;
		}
		
		/**
		 * Adds a deep copy of the provided featureValueDatabase to the Builder
		 * @param featureValueDatabase
		 * @return The updated Builder object, incorporating a deep copy of the passed featureValueDatabase
		 */

		public Builder featureValueDatabase (FeatureValueDatabase featureValueDatabase){
			this.featureValueDatabase = new FeatureValueDatabase(featureValueDatabase.getFeatureValueRecords());
			return this;
		}
		
		/**
		 * 
		 * @return	a deep copy of the feature value database
		 */
		public FeatureValueDatabase getFeatureValueDatabase(){
			return new FeatureValueDatabase(featureValueDatabase.getFeatureValueRecords());
		}
		
		
		/**
		 * Adds a deep copy of the provided map to the Builder object 
		 * @param featureValueExtractors
		 * @return The updated Builder object, incorporating a deep copy of the passed map
		 */
		public Builder featureValueExtractorMap(HashMap<Feature, FeatureValueExtractor> featureValueExtractorMap){
			this.featureValueExtractorMap = new HashMap<Feature, FeatureValueExtractor>();
			for(Feature feat: featureValueExtractorMap.keySet()){
				this.featureValueExtractorMap.put(feat, featureValueExtractorMap.get(feat));
			}
			return this;
		}

		/**
		 * 
		 * @return a deep copy of the hashmap of feature value extractors
		 */
		public HashMap<Feature, FeatureValueExtractor> getFeatureValueExtractorMap(){
			HashMap<Feature, FeatureValueExtractor> deepCopy = new HashMap<Feature, FeatureValueExtractor>();
			for(Feature feature: featureValueExtractorMap.keySet()){
				deepCopy.put(feature, featureValueExtractorMap.get(feature));
			}
			return deepCopy;
		}
 
		/**
		 * Creates a new algorithm selection system based on the current state of the system
		 * @return: the initialised algorithm selection system
		 * @throws FileNotFoundException: 	the directory in which the result files should be saved does not exist
		 * @throws InvalidSelectionMapException: the proposed selection mapping is not compatible with the other fields
		 */
		public AlgorithmSelectionSystem build() throws FileNotFoundException, InvalidSelectionMapException{
			return new AlgorithmSelectionSystem(this);			
		}
		
	}
}
