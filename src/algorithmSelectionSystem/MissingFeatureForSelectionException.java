package algorithmSelectionSystem;


public class MissingFeatureForSelectionException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1845081804664843818L;
	private Feature feature;
	private SelectionMap selectionMap;
	
	public MissingFeatureForSelectionException(Feature feature, SelectionMap selectionMap){
		super("Feature " + feature + " is required for a selection with selection map: "+ selectionMap);
		
		this.feature = feature;
		this.selectionMap = selectionMap;
	}
	
	public Feature getFeature(){
		return feature;
	}
	
	public SelectionMap getSelectionMap(){
		return selectionMap;
	}
}
