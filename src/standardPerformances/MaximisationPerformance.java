package standardPerformances;
import performanceDatabase.Performance;

/**
 * 
 * @author Hans Degroote
 * Represents a performance for problems where the goal is to maximise an objective value
 * The objective value can be used directly for comparing maximisation performances, 
 *  as it conforms to the contract of Performance: a higher value means a better performance
 *
 */
public class MaximisationPerformance extends Performance {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	double objectiveValue;
	
	public MaximisationPerformance(double objectiveValue){
		this.objectiveValue = objectiveValue;
	}
	
	public double getObjectiveValue(){
		return objectiveValue;
	}
	
	@Override
	public double getValue() {
		return objectiveValue;
	}
	
	@Override
	public String toString(){
		return ""+getObjectiveValue();
	}

	
}
