package standardPerformances;

import performanceDatabase.Performance;

public class MinimisationPerformance extends Performance  {

	/**
	 * @author Hans Degroote
	 * Represents a performance for problems where the goal is to minimise an objective value
	 * The objective value is negated when comparing minimisation performances, 
	 *  as it conforms to the contract of Performance: a higher value means a worse performance
	 */
	private static final long serialVersionUID = 1L;
	private double objectiveValue;
	public final static double INFEASIBLE_OBJECTIVE_VALUE = Double.POSITIVE_INFINITY;
	
	public MinimisationPerformance(double objectiveValue){
		this.objectiveValue = objectiveValue;
	}
	
	
	
	@Override
	/**
	 * Larger values must correspond to bigger performances. 
	 * Objective values have the opposite interpretation for min problems: 
	 * 	the smaller (-> typically the closer to zero) the better 
	 * Solution: negation. All performances will be negative (assuming zero is the minimum)
	 *  but the more negative a performance is, the worse it is 
	 */
	public double getValue() {
		return -objectiveValue;
	}
	
	public double getObjectiveValue(){
		return objectiveValue;
	}


	@Override
	public String toString(){
		return ""+getObjectiveValue();
	}

}
