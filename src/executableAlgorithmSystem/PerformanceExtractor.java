package executableAlgorithmSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import performanceDatabase.Performance;

/**
 * 
 * @author Hans Degroote
 * Represents a performance extractor, that extracts the performance from
 *  the result file of an algorithm
 * Whenever an algorithm is run, the output is written to a file, 
 *  implementations of this interface provide functionality to scan
 *  that result file to extract (or calculate) the performance
 *
 */
public abstract class PerformanceExtractor {

	
	/**
	 * Template method for extracting the performance from the outputfile of (presumably) an ExecutableInstance
	 * @param resFile the file from which performance should be extracted
	 * @return A Performance object containing the performance described in the given file
	 * @throws InvalidFileForPerformanceExtractionException if no performance can be extracted from the resFile
	 * @throws IllegalArgumentException if resFile is null
	 * @throw FileNotFoundException if the specified resFile cannot be opened
	 * @throws IOException if something went wrong while reading the file
	 */
	public Performance extractPerformance(File resFile) throws 
				InvalidFileForPerformanceExtractionException, IOException, IllegalArgumentException, FileNotFoundException{
		if(resFile == null) throw new IllegalArgumentException("File from which performance should be extracted cannot be null");
		if(!resFile.exists()) throw new FileNotFoundException("File " + resFile + " from which performance was attempted to be extracted was not found");
		
		return extractPerformanceImplementation(resFile);
		
	}
	
	/**
	 * Method that needs to be implemented by all PerformanceExtractor
	 *  that extracts the performance from the given resFile
	 * It should never be called explicitly. It is called automatically
	 *  by the extractPerformance method of PerformanceExtractor, where it 
	 *  is used as the implementation-specific functionality for extracting performance
	 * @param resFile: the file from which performance should be extracted
	 * @Pre: resFile is a valid File object, referring to an existing file
	 * @return A Performance object containing the performance described in the given file
	 */
	protected abstract Performance extractPerformanceImplementation(File resFile) throws InvalidFileForPerformanceExtractionException, IOException ;
}
