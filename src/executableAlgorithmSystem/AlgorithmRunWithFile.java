package executableAlgorithmSystem;

import java.io.File;
import java.io.FileNotFoundException;

import performanceDatabase.Algorithm;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.Instance;
import performanceDatabase.Performance;

/**
 * 
 * @author Hans Degroote
 * Represents an algorithm run for which a result file is available on the file system
 *
 */
public class AlgorithmRunWithFile extends AlgorithmRun {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	File resultFile;
	
	public AlgorithmRunWithFile(Algorithm algorithm, Instance instance, Performance performance, File resultFile)
			throws IllegalArgumentException, FileNotFoundException {
		super(algorithm, instance, performance);
		if(resultFile == null) throw new IllegalArgumentException("resultFile cannot be null");
		if(!resultFile.exists()) throw new FileNotFoundException("The resultFile must exist: " + resultFile);
		this.resultFile = new File(resultFile.getAbsolutePath());
	}
	
	public File getResultFile(){
		return new File(resultFile.getAbsolutePath());
	}
	
	@Override
	public String toString(){
		String res = super.toString();
		res += "\nResult file: " + resultFile.toString();
		return res;
	}

}
