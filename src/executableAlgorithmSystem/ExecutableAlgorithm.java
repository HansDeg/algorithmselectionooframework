package executableAlgorithmSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;

import performanceDatabase.Algorithm;



/**
 * 
 * @author Hans Degroote
 * Defines an algorithm that can be executed
 * Meaning that the algorithm object is linked with an executable that 
 * can be run on instances corresponding to the problem for which the algorithm is defined
 *
 */
public class ExecutableAlgorithm extends Algorithm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The executable corresponding to the algorithm
	 */
	private File executableFile;
	/**
	 * Parameters and their values that should be passed to the executable
	 * If inputFilePassedAsParameter == true, then one of the locations should be reserved for the instance
	 * to be passed. This location is stored in the field 'positionOfInputFileInParameterList'
	 */
	private ArrayList<String> parameterList;
	
	/**
	 * Optional: an inputFileTransformer object that transforms a standard input file
	 * to a format that can be read by this algorithm
	 */

	private InputFileTransformer inputFileTransformer;
	
	/**
	 * True if the input file should be passed as a parameter
	 * False if the input is read from the standard input stream
	 */
	private boolean inputFilePassedAsParameter;
	/**
	 * The position of the input file in the list of parameters
	 * Only relevant if inputFilePassedAsParameter==true
	 */
	private int positionOfInputFileInParameterList;	
	/**
	 * True if the output file should be passed as a parameter
	 * False if the output is read from the standard output stream
	 */
	private boolean outputFilePassedAsParameter; 
	/**
	 * The position of the output file in the list of parameters
	 * Only relevant if outputFilePassedAsParameter==true
	 */
	private int positionOfOutputFileInParameterList;
	
	/**
	 * Should a seet be passed to the algorithm when calling it?
	 */
	private boolean hasSeed;
	
	/**
	 * The command that indicates a seed follows. 
	 */
	private String seedCommand;
	
	/**
	 * The prefixes that should be put on the command line before calling the executable
	 */
	ArrayList<String> prefixesList;
	
	/**
	 * 	Constructor implemented using the builder pattern
	 * @param builder the builder object used to initialise the executable algorithm
	 * @throws FileNotFoundException: the executable file was not found
	 * @throws IllegalArgumentException: one of the arguments was invalid (either it was null or some other problem)
	 */
	protected ExecutableAlgorithm(Builder builder) throws FileNotFoundException, IllegalArgumentException{
		super(builder.name);
		if(builder.executableFile == null) throw new IllegalArgumentException("executableFile cannot be null");
		if(!builder.executableFile.exists()) throw new FileNotFoundException("The executable file was not found: " + builder.executableFile);
		if(builder.parameterList == null) throw new IllegalArgumentException("parameterString cannot be null. Pass empty string for no parameters");
		if(builder.inputFilePassedAsParameter && builder.positionOfInputFileInParameterList <0) throw new IllegalArgumentException("The position of the input file in the parameter list must not be negative, but " + builder.positionOfInputFileInParameterList + " was passed");
		if(builder.inputFilePassedAsParameter && builder.positionOfInputFileInParameterList 
				>= builder.parameterList.size()) throw new IllegalArgumentException("Position of input file argument is outside of the bounds of the parameter list");
		if(builder.outputFilePassedAsParameter && builder.positionOfOutputFileInParameterList <0) throw new IllegalArgumentException("The position of the output file in the parameter list must not be negative, but " + builder.positionOfInputFileInParameterList + " was passed");
		if(builder.outputFilePassedAsParameter && builder.positionOfOutputFileInParameterList 
				>= builder.parameterList.size()) throw new IllegalArgumentException("Position of output file argument is outside of the bounds of the parameter list");
		if(builder.hasSeed && builder.seedCommand == null) throw new IllegalArgumentException("No seed command was specified, but it was specified the algorithm should have a seed");
		if(builder.prefixesList == null) throw new IllegalArgumentException("The list of prefixes cannot be null");
		
		this.executableFile = new File(builder.executableFile.getAbsolutePath());	//Deep copy, unsure if necessary	
		this.parameterList = new ArrayList<String>(builder.parameterList);
		this.inputFilePassedAsParameter = builder.inputFilePassedAsParameter;
		this.inputFileTransformer = builder.inputFileTransformer;
		this.positionOfInputFileInParameterList = builder.positionOfInputFileInParameterList;
		this.positionOfOutputFileInParameterList = builder.positionOfOutputFileInParameterList;
		this.outputFilePassedAsParameter = builder.outputFilePassedAsParameter;
		this.hasSeed = builder.hasSeed;
		this.seedCommand = builder.seedCommand;
		this.prefixesList = new ArrayList<String>(builder.prefixesList);
	}

	
	/*public ExecutableAlgorithm(String name, File executableFile, ArrayList<String> parameterList, boolean inputFilePassedAsParameter, 
			InputFileTransformer inputFileTransformer, int positionOfInputFileInParameterList) throws IllegalArgumentException, FileNotFoundException{
		super(name);
		if(executableFile == null) throw new IllegalArgumentException("executableFile cannot be null");
		if(!executableFile.exists()) throw new FileNotFoundException("The executable file was not found: " + executableFile);
		if(parameterList == null) throw new IllegalArgumentException("parameterString cannot be null. Pass empty string for no parameters");
		if(inputFilePassedAsParameter && positionOfInputFileInParameterList <0) throw new IllegalArgumentException("The position of the input file in the parameter list cannot be lower than 0");
		
		this.executableFile = new File(executableFile.getAbsolutePath());	//Deep copy, unsure if necessary	
		this.parameterList = parameterList;
		this.inputFilePassedAsParameter = inputFilePassedAsParameter;
		this.inputFileTransformer = inputFileTransformer;
		this.positionOfInputFileInParameterList = positionOfInputFileInParameterList;
	}*/

	
	/**
	 * Creates an executableAlgorithm without parameters
	 * @param name
	 * @param executableFile
	 * @throws IllegalArgumentException
	 */
	/*public Algorithm(String name, File executableFile ) throws IllegalArgumentException {
		this(name, executableFile, new ArrayList<String>());
	}*/
	
	public int getPositionOfInputFileInParameterList(){
		return positionOfInputFileInParameterList;
	}
	
	/**
	 * 
	 * @return a deep copy of the parameter list
	 */
	public ArrayList<String> getParameterList(){
		return new ArrayList<String>(parameterList);
	}

	public InputFileTransformer getInputFileTransformer(){
		return inputFileTransformer;
	}

	public boolean isInputFilePassedAsParameter(){
		return inputFilePassedAsParameter;
	}

	public File getExecutableFile(){
		return new File(executableFile.getAbsolutePath()); //Deep copy, not sure if needed
	}
	
	public boolean isOutputFilePassedAsParameter(){
		return outputFilePassedAsParameter;
	}
	
	public int getPositionOfOutputFileInParameterList(){
		return positionOfOutputFileInParameterList;
	}
	
	public boolean hasSeed(){
		return hasSeed;
	}
	
	public String getSeedCommand(){
		return seedCommand;
	}

	

	/**
	 * Creates a ProcessBuilder object, from which a process can be started that executes
	 * this algorithm on the specified instance. 
	 * This method supposes no seed should be specified. If a seed should be specified,
	 * it will always be given the same value '-1'.
	 * The way in which the process is build, and especially the parameters it receives
	 *  is decided based on the fields of the ExecutableAlgorithm object
	 *  
	 *  Step 1: 
	 *  	If instance file needs be transformed: transform it
	 *  Step 2:
	 *  	If instance is passed as a parameter, add it to the correct position
	 *  	in the parameter list
	 *  	Equal for output file
	 *  Step 3:
	 *  	If the executable is an executable jar: call it as an exec jar should (java -jar execName)
	 *  	If not: call it as a standard executable (execName)
	 *  Step 4:
	 *  	If the input file is not passed as a parameter (meaning input is passed through standard input)
	 *  	change the input-stream for the process to the file
	 *  
	 *  	
	 *  
	 * @param instance	The instance on which the algorithm should be executed
	 * @param outputFile	The file where the output of the algorithm should be saved
	 * @param logFile	The file where error and non-output related standard output should be written to
	 * @return a ProcessBuilder object that when executed, runs this algorithm's executable on the given instance
	 */
	public ProcessBuilder buildProcessFor(RunnableInstance instance, File outputFile, File logFile) throws IllegalArgumentException{
		//The third argument (seed) is never used for algorithms who do not have a seed
		//If they do have a seed, it will always have value -1
		return buildProcessFor(instance, outputFile, logFile, -1); 
	}

	/**
	 * Creates a ProcessBuilder object, from which a process can be started that executes
	 * this algorithm on the specified instance. 
	 * The way in which the process is build, and especially the parameters it receives
	 *  is decided based on the fields of the ExecutableAlgorithm object
	 *  
	 *  Step 1: 
	 *  	If instance file needs be transformed: transform it
	 *  Step 2:
	 *  	If instance is passed as a parameter, add it to the correct position
	 *  	in the parameter list
	 *  	Equal for output file
	 *  Step 3:
	 *  	If the algorithm expects a random seed as parameter, add it to the
	 *		parameter list
	 *  Step 4:
	 *  	If the executable is an executable jar: call it as an exec jar should (java -jar execName)
	 *  	If not: call it as a standard executable (execName)
	 *  Step 5:
	 *  	If the input file is not passed as a parameter (meaning input is passed through standard input)
	 *  	change the input-stream for the process to the file
	 *  Step 6:
	 *  	The working directory for the command prompt is set to the directory in which the executable file is held 
	 *  Step 7: 
	 *  	Error and (if an output file is specified explicitly) standard output are redirected to a log file
	 *  
	 * @param instance	The instance on which the algorithm should be executed
	 * @param outputFile	The file where the output of the algorithm should be saved
 	 * @param logFile	The file where error and non-output related standard output should be written to
	 * @param seed	The seed with which the algorithm should be initiated for this run
	 * @return a ProcessBuilder object that when executed, runs this algorithm's executable on the given instance
	 */
	public ProcessBuilder buildProcessFor(RunnableInstance instance, File outputFile, File logFile, long seed) throws IllegalArgumentException{
		if(instance == null) throw new IllegalArgumentException("instance cannot be null");
		
		String execPath = executableFile.getAbsolutePath();
		ProcessBuilder pb = null;
		ArrayList<String> parameters = getParameterList(); 
		String instPath = null; 
		
		//Step 1
		if(inputFileTransformer != null){ //Actual input file needs to be transformed before being fed to the algorithm
			instPath = inputFileTransformer.transformInputFile(instance).getAbsolutePath();
		}
		else{ //No need for transformation
			instPath = instance.getInputFile().getAbsolutePath();
		}
		
		//Step 2
		//If the input file is passed as a parameter, it is added to the parameter list
		//on the position specified by positionOfInputFileInParameterList
		if(inputFilePassedAsParameter){ //Logical else of this statement is defined only after the pb is initialised
			parameters.set(positionOfInputFileInParameterList, instPath);
		}
		
		//If the output file is passed as a parameter, is is added to the parameter list
		//on the position specified by positionOfOutputFileInParameterList
		//and any additional output is rerouted to a standard log file
		if(outputFilePassedAsParameter){
			parameters.set(positionOfOutputFileInParameterList, outputFile.getAbsolutePath());
		}
		
		//Step 3 If the algorithm expects a seed, give it the passed value
		if(hasSeed()){
			parameters.add(getSeedCommand());
			parameters.add(""+seed);
		}
		
		//Step 4 
		//add executable and its prefixes
		parameters.add(0, execPath);
		parameters.addAll(0, prefixesList);
		
		pb = new ProcessBuilder(parameters);
		
		//Step 5
		if(!inputFilePassedAsParameter){ //File must be piped into process
			pb.redirectInput(instance.getInputFile());
		}
		pb.redirectErrorStream(true);
		if(!outputFilePassedAsParameter){ //output must be redirected to the output file
			pb.redirectOutput(Redirect.to(outputFile));
		}
		
		//step 6: 
		File execFile = new File(execPath);
		pb.directory(execFile.getParentFile());
		
		//Step 7:
		pb.redirectError(Redirect.to(logFile));
		if(outputFilePassedAsParameter){ //If not output is already redirected there anyway
			pb.redirectOutput(Redirect.to(logFile));
		}
		return pb;
		
		
	}



	
	
	/**
	 * 
	 * @author Hans Degroote
	 * Builder pattern for an ExecutableAlgorithm
	 *
	 */
	public static class Builder{

		private String name;
		private File executableFile;
		private ArrayList<String> parameterList;
		private boolean inputFilePassedAsParameter;
		private InputFileTransformer inputFileTransformer;
		private int positionOfInputFileInParameterList;
		private boolean outputFilePassedAsParameter;
		private int positionOfOutputFileInParameterList;
		private boolean hasSeed;
		private String seedCommand;
		private ArrayList<String> prefixesList;
		
		/**
		 * Initialises the builder for creating an executable algorithm based on the
		 * 		specified name and executableFile
		 * The builder is initialised to create an algorithm for which
		 * 	-input file is passed as a parameter
		 * 	-The input file is passed as the first parameter
		 * 	-no other parameters are passed
		 * 	-no input file transformer is used
		 * parameterList is initialised with 1 
		 * inputFilePassedAsParameter is initialised true
		 * positionOfInputFileInParameterList is initialised -1 (meaning the
		 * 		input file is appended as last parameter)
		 * inputFileTransformer is initialised null (meaning there is none)
		 * outputFilePassedAsParameter is initialised false (meaning the exec prints its output on the standard out)
		 * positionOfOutputFileInParameterList is initialised to -1
		 * hasSeed is initialised to false
		 * seedCommand is initialised to null
		 * prefixesList is initialised to the empty list
		 * 
		 * @param name: the name of the algorithm
		 * @param executableFile: the executable file corresponding to the algorithm
		 */
		public Builder(String name, File executableFile){
			this.name = name;
			this.executableFile = executableFile;
			this.parameterList = new ArrayList<String>();
			this.parameterList.add("instance"); //the standard positionOfInputFileInParameterList is 0 
			this.inputFilePassedAsParameter=true;
			this.positionOfInputFileInParameterList = 0;
			this.inputFileTransformer = null;
			this.outputFilePassedAsParameter=false;
			this.positionOfOutputFileInParameterList = -1;
			this.hasSeed = false;
			this.seedCommand = null;
			this.prefixesList = new ArrayList<String>();
		}
		
		public Builder seedCommand(String command){
			this.seedCommand = command;
			this.hasSeed = true;
			return this;
		}
		
		public Builder parameterList(ArrayList<String> parameterList){
			if(parameterList != null){
				this.parameterList = new ArrayList<String>(parameterList);
			}
			else{
				this.parameterList = null; 
			}
			return this;
		}
		
		public Builder inputFilePassedAsParameter(boolean inputFilePassedAsParameter){
			this.inputFilePassedAsParameter = inputFilePassedAsParameter;
			return this;
		}
		
		/**
		 *
		 * @param 	positionOfInputFileInParameterList: the position of the input file in the parameter list
		 *  		Input -1 to append it as last element
		 * @return 	a Builder object with updated positionOfInputFileInParameterList
		 */
		public Builder positionOfInputFileInParameterList(int positionOfInputFileInParameterList){
			this.positionOfInputFileInParameterList = positionOfInputFileInParameterList;
			return this;
		}
		
		public Builder inputFileTransformer(InputFileTransformer inputFileTransformer){
			this.inputFileTransformer = inputFileTransformer;
			return this;
		}
		
		
		public Builder outputFilePassedAsParameter(boolean outputFilePassedAsParameter){
			this.outputFilePassedAsParameter = outputFilePassedAsParameter;
			return this;
		}
		
		public Builder positionOfOutputFileInParameterList(int positionOfOutputFileInParameterList){
			this.positionOfOutputFileInParameterList = positionOfOutputFileInParameterList;
			return this;
		}
		
		public Builder prefixesList(ArrayList<String> prefixesList){
			this.prefixesList = new ArrayList<String>(prefixesList);
			return this;
		}
		
		
		public ExecutableAlgorithm build() throws IllegalArgumentException, FileNotFoundException{
			return new ExecutableAlgorithm(this);	
			
		}
		
	}
	
}
