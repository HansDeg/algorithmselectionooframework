package executableAlgorithmSystem;

import performanceDatabase.Algorithm;

/**
 * 
 * @author Hans Degroote
 * 
 * Represents an exception thrown when an algorithm could not be found
 *
 */
public class AlgorithmNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Algorithm algorithm;
	
	public AlgorithmNotFoundException(Algorithm algorithm){
		this.algorithm = algorithm;
	}
	
	@Override 
	public String toString(){
		return algorithm + "was not found";
	}
}
