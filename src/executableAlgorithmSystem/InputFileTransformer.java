package executableAlgorithmSystem;

import java.io.File;

/**
 * 
 * @author Hans Degroote
 * An interface for preprocessing the input of a runnable instance
 * Returns a file that is created based on the passed instance's inputfile
 * Used for algorithms who cannot directly read the standard input format
 *  
 *
 */
public interface InputFileTransformer{
	
	/**
	 * 
	 * @param instance: the runnable instance for which the input file should be transformed
	 * @return: a transformed version of the instance's input file, that can
	 *  be read by the executable algorithm for which the transformer is created
	 */
	public File transformInputFile(RunnableInstance instance);
}

