package executableAlgorithmSystem;

import java.io.File;
import java.io.FileNotFoundException;

import performanceDatabase.Instance;

/**
 * 
 * @author Hans Degroote
 * Represents an instance that can be executed by an algorithm
 * It extends the standard Instance concept by adding a reference to a file.
 *  The file contains the specification of the instance that can be fed as input to an algorithm
 */
public class RunnableInstance extends Instance  {

	private static final long serialVersionUID = 1L;
	
	/**
	 * The file containing the instance specification (that can be used by algorithms as input)
	 */
	private File inputFile;
	
	public RunnableInstance(String name, File inputFile) throws IllegalArgumentException, FileNotFoundException {
		super(name);
		if(inputFile == null) throw new IllegalArgumentException("Inputfile cannot be null");
		if(!inputFile.exists()) throw new FileNotFoundException("The inputFile " + inputFile + " was not found");
		this.inputFile = new File(inputFile.getAbsolutePath());	//Deep copy, unsure if necessary	
	}
	
	/**
	 * 
	 * @return a deep copy of the inputFile
	 */
	public File getInputFile(){
		return new File(inputFile.getAbsolutePath()); //Deep copy (not sure if useful here)
	}

}



