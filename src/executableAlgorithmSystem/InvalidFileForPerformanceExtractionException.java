package executableAlgorithmSystem;

import java.io.File;

/**
 * 
 * @author Hans Degroote
 * Exception that should be used when a file has been passed to 
 *  a performance extractor from which it fails to extract a performance
 *  The implication is that either the Performance extractor itself is faulty
 *  or the passed file was not the result file of the target algorithm
 * 
 *
 */
public class InvalidFileForPerformanceExtractionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8791873095781760560L;

	private File file;
	private PerformanceExtractor extractor;
	
	public InvalidFileForPerformanceExtractionException(File file, PerformanceExtractor extractor){
		this("", file, extractor);
	}
	
	public InvalidFileForPerformanceExtractionException(String message, File file, PerformanceExtractor extractor){
		super(message + "No performance could be extracted from file " + file + " using extractor " + extractor);
		this.file = new File(file.getAbsolutePath());
		this.extractor = extractor;
	}

	
	public File getFile(){
		return new File(file.getAbsolutePath());
	}
	
	public PerformanceExtractor getPerformanceExtractor(){
		return extractor;
	}
	
}
