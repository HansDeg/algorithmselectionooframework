package standardSelectionMapInitialisationStrategies;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import algorithmSelectionSystem.Feature;
import algorithmSelectionSystem.FeatureValueDatabase;
import executableAlgorithmSystem.ExecutableAlgorithm;
import onlineAlgorithmSelection.SelectionMapInitialisationStrategy;
import performanceDatabase.AlgorithmRun;
import performanceDatabase.AlgorithmRunDatabase;
import standardSelectionMaps.SingleAlgorithmSelectionMap;

/**
 * 
 * @author Hans Degroote
 * Pick observed best single best solver strategy
 * Selection map initialisation strategy that returns a single algorithm selection map (always selecting the same algorithm)
 * The algorithm to always select is the algorithm observed best in the past
 * Features are ignored by this strategy.
 *
 */
public class PickObservedSbsStrategy implements SelectionMapInitialisationStrategy, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Random rNrGen;
	
	public PickObservedSbsStrategy(){
		rNrGen = new Random();
	}
	
	
	
	/**
	 * Return a selection map that always picks one algorithm. That one algorithm
	 * 	is the single best solver as observed on the passed algorithmRunDatabase
	 * 	An algorithm for which no runs are available gets performance -Infinity and will never 
	 * 	be selected unless no performance is available for any algorithms
	 * 	Draws are broken randomly
	 * 	
	 */
	@Override
	public SingleAlgorithmSelectionMap getSelectionMap(Set<ExecutableAlgorithm> availableAlgorithms, Set<Feature> availableFeatures,
			AlgorithmRunDatabase algorithmRunDatabase,FeatureValueDatabase featureValueDatabase) {
		
		ExecutableAlgorithm bestAlg = null;
		double bestPerf = Double.NEGATIVE_INFINITY;
		
		
		for(ExecutableAlgorithm alg: availableAlgorithms){
			double summedPerf = 0;
			double avgPerf;
			HashSet<AlgorithmRun> thisAlgRuns = algorithmRunDatabase.getAllRunsOfAlgorithm(alg);
			if(thisAlgRuns.size() == 0){
				avgPerf = Double.NEGATIVE_INFINITY;
			}
			else{
				for(AlgorithmRun run: thisAlgRuns ){
					summedPerf += run.getPerformance().getValue();
				}
				avgPerf = summedPerf/thisAlgRuns.size();
			}
			if(avgPerf > bestPerf){
				bestPerf = avgPerf;
				bestAlg = alg;
			}
		}
		
		//Checking that a non-negative-infinity performance exists for at least one algorithm.
		//If not: return random algorithm
		if(bestPerf == Double.NEGATIVE_INFINITY){
			ArrayList<ExecutableAlgorithm> listOfAlgs = new ArrayList<ExecutableAlgorithm>(availableAlgorithms);
			int algId = rNrGen.nextInt(listOfAlgs.size());
			ExecutableAlgorithm randomAlg = listOfAlgs.get(algId);
			SingleAlgorithmSelectionMap res = new SingleAlgorithmSelectionMap(randomAlg); 
			return res;
		}
		else{
			SingleAlgorithmSelectionMap res = new SingleAlgorithmSelectionMap(bestAlg);
			return res;
		}
	}
	
	@Override
	public String toString(){
		return "Always select observed single best solver selection mapping initialisation strategy";
	}

	
	

}